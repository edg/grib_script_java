package grib_script.ast.visitor;

import grib_script.ast.node.*;

public interface ExpressionVisitor<T> {
    T visit(BinaryOperationExpression expression);

    T visit(BooleanExpression expression);

    T visit(DoubleExpression expression);

    T visit(Function expression);

    T visit(IntegerExpression expression);

    T visit(StringExpression expression);

    T visit(VariableExpression expression);

    T visit(UnaryOperationExpression unaryOperationExpression);
}