package grib_script.ast.visitor;

import grib_script.ast.node.Assignment;
import grib_script.ast.node.Function;
import grib_script.ast.node.IfElse;

public interface StatementVisitor<T> {
    T visit(Assignment assignment);

    T visit(Function function);

    T visit(IfElse ifElse);
}
