package grib_script.ast.visitor;

import grib_script.ast.node.*;

public interface SectionVisitor<T> {
    T visit(BeginSection section);

    T visit(EndSection section);

    T visit(MatchSection section);
}
