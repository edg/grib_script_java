package grib_script.ast.node;

import grib_script.ast.visitor.StatementVisitor;

import java.util.ArrayList;
import java.util.List;

public class IfElse implements Statement {
    private final List<IfBlock> ifBlocks = new ArrayList<>();

    public boolean add(IfBlock ifBlock) {
        return ifBlocks.add(ifBlock);
    }

    public List<IfBlock> getIfBlocks() {
        return ifBlocks;
    }

    @Override
    public <T> T accept(StatementVisitor<? extends T> visitor) {
        return visitor.visit(this);
    }
}
