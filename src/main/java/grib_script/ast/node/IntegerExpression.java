package grib_script.ast.node;

import grib_script.ast.visitor.ExpressionVisitor;

public class IntegerExpression implements Expression {
    private final int value;

    public IntegerExpression(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public <T> T accept(ExpressionVisitor<? extends T> visitor) {
        return visitor.visit(this);
    }
}
