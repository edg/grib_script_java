package grib_script.ast.node;

import grib_script.ast.visitor.ExpressionVisitor;

public class BinaryOperationExpression implements Expression {
    private final Expression left;
    private final Expression right;
    private final BinaryOperationType operation;

    public BinaryOperationExpression(Expression left, Expression right, BinaryOperationType operation) {
        this.left = left;
        this.right = right;
        this.operation = operation;
    }

    public Expression getLeft() {
        return left;
    }

    public Expression getRight() {
        return right;
    }

    public BinaryOperationType getOperation() {
        return operation;
    }

    @Override
    public <T> T accept(ExpressionVisitor<? extends T> visitor) {
        return visitor.visit(this);
    }
}
