package grib_script.ast.node;

import java.util.ArrayList;
import java.util.List;

public class Program {
    private final List<Section> sections = new ArrayList<>();

    public void add(Section section) {
        sections.add(section);
    }

    public List<Section> getSections() {
        return sections;
    }
}
