package grib_script.ast.node;

import grib_script.ast.visitor.StatementVisitor;

public interface Statement {
    <T> T accept(StatementVisitor<? extends T> visitor);

}
