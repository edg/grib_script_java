package grib_script.ast.node;

import grib_script.ast.visitor.SectionVisitor;

import java.util.ArrayList;
import java.util.List;

public abstract class Section {
    private final List<Statement> statements = new ArrayList<>();

    public List<Statement> getStatements() {
        return statements;
    }

    public abstract <T> T accept(SectionVisitor<? extends T> visitor);
}
