package grib_script.ast.node;

import grib_script.ast.visitor.ExpressionVisitor;

public class UnaryOperationExpression implements Expression {
    private final Expression expression;
    private final UnaryOperationType operationType;

    public UnaryOperationExpression(Expression expression, UnaryOperationType operationType) {
        super();
        this.expression = expression;
        this.operationType = operationType;
    }

    public Expression getExpression() {
        return expression;
    }

    public UnaryOperationType getOperationType() {
        return operationType;
    }

    @Override
    public <T> T accept(ExpressionVisitor<? extends T> visitor) {
        return visitor.visit(this);
    }

}
