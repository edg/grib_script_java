package grib_script.ast.node;

import grib_script.ast.visitor.ExpressionVisitor;

public class StringExpression implements Expression {
    private final String value;
    private final String unquotedValue;

    public StringExpression(String value) {
        this.value = value;
        this.unquotedValue = value.replaceAll("\"", "").replaceAll("\\\\(.)", "$1");
    }

    public String getValue() {
        return this.value;
    }

    public String getUnquotedValue() {
        return this.unquotedValue;
    }

    @Override
    public <T> T accept(ExpressionVisitor<? extends T> visitor) {
        return visitor.visit(this);
    }
}
