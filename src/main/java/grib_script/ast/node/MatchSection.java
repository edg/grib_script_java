package grib_script.ast.node;

import grib_script.ast.visitor.SectionVisitor;

public class MatchSection extends Section {
    private final Expression condition;

    public MatchSection(Expression condition) {
        this.condition = condition;
    }

    public Expression getCondition() {
        return this.condition;
    }

    @Override
    public <T> T accept(SectionVisitor<? extends T> visitor) {
        return visitor.visit(this);
    }

}
