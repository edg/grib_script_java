package grib_script.ast.node;

import java.util.ArrayList;
import java.util.List;

public class IfBlock {
    private final Expression condition;

    private final List<Statement> statements = new ArrayList<>();

    public IfBlock(Expression condition) {
        this.condition = condition;
    }

    public boolean add(Statement statement) {
        return statements.add(statement);
    }

    public Expression getCondition() {
        return condition;
    }

    public List<Statement> getStatements() {
        return this.statements;
    }
}
