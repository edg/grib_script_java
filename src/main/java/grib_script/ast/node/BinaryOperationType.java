package grib_script.ast.node;

public enum BinaryOperationType {
    SUM, SUB, MUL, DIV, AND, OR, EQ, NEQ, LT, LTE, GT, GTE
}
