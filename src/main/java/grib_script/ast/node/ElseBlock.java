package grib_script.ast.node;

public class ElseBlock extends IfBlock {
    public ElseBlock() {
        super(new BooleanExpression(true));
    }
}
