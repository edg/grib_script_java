package grib_script.ast.node;

import grib_script.ast.visitor.SectionVisitor;

public class BeginSection extends Section {
    @Override
    public <T> T accept(SectionVisitor<? extends T> visitor) {
        return visitor.visit(this);
    }
}
