package grib_script.ast.node;

import grib_script.ast.visitor.ExpressionVisitor;

public class DoubleExpression implements Expression {
    private final double value;

    public DoubleExpression(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public <T> T accept(ExpressionVisitor<? extends T> visitor) {
        return visitor.visit(this);
    }
}
