package grib_script.ast.node;

import grib_script.ast.visitor.StatementVisitor;

public class Assignment implements Statement {

    private final String name;
    private final Expression expression;

    public Assignment(String name, Expression expression) {
        this.name = name;
        this.expression = expression;
    }

    public String getName() {
        return name;
    }

    public Expression getExpression() {
        return expression;
    }

    @Override
    public <T> T accept(StatementVisitor<? extends T> visitor) {
        return visitor.visit(this);
    }

}
