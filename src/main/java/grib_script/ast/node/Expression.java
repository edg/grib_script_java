package grib_script.ast.node;

import grib_script.ast.visitor.ExpressionVisitor;

public interface Expression {
    public <T> T accept(ExpressionVisitor<? extends T> visitor);
}
