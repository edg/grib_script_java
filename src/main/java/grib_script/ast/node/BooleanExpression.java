package grib_script.ast.node;

import grib_script.ast.visitor.ExpressionVisitor;

public class BooleanExpression implements Expression {
    private final boolean value;

    public BooleanExpression(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }

    @Override
    public <T> T accept(ExpressionVisitor<? extends T> visitor) {
        return visitor.visit(this);
    }

}
