package grib_script.ast.node;

import grib_script.ast.visitor.ExpressionVisitor;
import grib_script.ast.visitor.StatementVisitor;

import java.util.List;

public class Function implements Expression, Statement {
    private final String name;
    private final List<Expression> args;

    public Function(String name, List<Expression> args) {
        this.name = name;
        this.args = args;
    }

    public String getName() {
        return name;
    }

    public List<Expression> getArgs() {
        return args;
    }

    @Override
    public <T> T accept(StatementVisitor<? extends T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public <T> T accept(ExpressionVisitor<? extends T> visitor) {
        return visitor.visit(this);
    }
}
