package grib_script.ast.node;

import grib_script.ast.visitor.ExpressionVisitor;

public class VariableExpression implements Expression {
    private final String name;

    public VariableExpression(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public <T> T accept(ExpressionVisitor<? extends T> visitor) {
        return visitor.visit(this);
    }
}
