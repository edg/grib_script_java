package grib_script.ast.builder;

import grib_script.antlr4.GribScriptBaseVisitor;
import grib_script.antlr4.GribScriptParser;
import grib_script.antlr4.GribScriptParser.AssignmentContext;
import grib_script.antlr4.GribScriptParser.FunctionStatementContext;
import grib_script.antlr4.GribScriptParser.IfElseContext;
import grib_script.ast.node.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StatementBuilder extends GribScriptBaseVisitor<Statement> {
    @Override
    public Statement visitAssignment(AssignmentContext ctx) {
        String name = ctx.ID().getText();
        ExpressionBuilder expressionBuilder = new ExpressionBuilder();
        Expression expression = ctx.expr().accept(expressionBuilder);
        return new Assignment(name, expression);
    }

    private IfBlock createIfElement(GribScriptParser.IfblockContext ifblock) {
        IfBlock ifBlock = new IfBlock(ifblock.expr().accept(new ExpressionBuilder()));
        populateIfElementStatements(ifBlock, ifblock.ifstatements());
        return ifBlock;
    }

    private void populateIfElementStatements(IfBlock ifBlock, GribScriptParser.IfstatementsContext ctx) {
        ctx.statements().statement().stream().map(s -> s.accept(new StatementBuilder())).forEachOrdered(ifBlock::add);
    }

    @Override
    public Statement visitIfElse(IfElseContext ctx) {
        // Add if and elifs
        IfElse ifElseStatement = new IfElse();
        ctx.ifelse().ifblock().stream().map(this::createIfElement).forEachOrdered(ifElseStatement::add);
        // Add else
        if (ctx.ifelse().elsestatements() != null) {
            IfBlock elseBlock = new ElseBlock();
            populateIfElementStatements(elseBlock, ctx.ifelse().elsestatements().ifstatements());
            ifElseStatement.add(elseBlock);
        }
        return ifElseStatement;
    }

    @Override
    public Statement visitFunctionStatement(FunctionStatementContext ctx) {
        List<Expression> args = new ArrayList<>();
        if (ctx.function().functionargs() != null) {
            args = ctx.function().functionargs().expr().stream().map(e -> e.accept(new ExpressionBuilder()))
                    .collect(Collectors.toList());
        }
        return new Function(ctx.function().ID().getText(), args);
    }
}
