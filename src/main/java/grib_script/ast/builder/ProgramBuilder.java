package grib_script.ast.builder;

import grib_script.antlr4.GribScriptBaseVisitor;
import grib_script.antlr4.GribScriptParser.ProgramContext;
import grib_script.antlr4.GribScriptParser.SectionContext;
import grib_script.ast.node.Program;
import grib_script.ast.node.Section;

/**
 * <p>
 * Questa classe genera l'AST (nodo radice {@link Program}) a partire dal CST
 * ({@link ProgramContext}).
 * </p>
 */
public class ProgramBuilder extends GribScriptBaseVisitor<Program> {
    /**
     * Genera l'AST a partire dal CST
     *
     * @param ctx the CST
     * @return the AST
     */
    @Override
    public Program visitProgram(ProgramContext ctx) {
        Program program = new Program();
        // Iterate over sections
        for (SectionContext sectionCtx : ctx.sections().section()) {
            SectionBuilder sectionBuilder = new SectionBuilder();
            Section section = sectionCtx.accept(sectionBuilder);
            program.add(section);
        }
        return program;
    }
}
