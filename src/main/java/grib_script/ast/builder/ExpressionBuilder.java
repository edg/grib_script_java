package grib_script.ast.builder;

import grib_script.antlr4.GribScriptBaseVisitor;
import grib_script.antlr4.GribScriptParser;
import grib_script.antlr4.GribScriptParser.AddSubExprContext;
import grib_script.antlr4.GribScriptParser.BoolExprContext;
import grib_script.antlr4.GribScriptParser.DoubleContext;
import grib_script.antlr4.GribScriptParser.IntContext;
import grib_script.antlr4.GribScriptParser.MulDivExprContext;
import grib_script.antlr4.GribScriptParser.StringContext;
import grib_script.antlr4.GribScriptParser.UnaryExprContext;
import grib_script.ast.node.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ExpressionBuilder extends GribScriptBaseVisitor<Expression> {
    @Override
    public Expression visitBoolExpr(BoolExprContext ctx) {
        Expression left = ctx.expr(0).accept(this);
        Expression right = ctx.expr(1).accept(this);
        BinaryOperatorBuilder binaryOperatorBuilder = new BinaryOperatorBuilder();
        BinaryOperationType operation = ctx.boolop().accept(binaryOperatorBuilder);
        return new BinaryOperationExpression(left, right, operation);
    }

    @Override
    public Expression visitMulDivExpr(MulDivExprContext ctx) {
        Expression left = ctx.expr(0).accept(this);
        Expression right = ctx.expr(1).accept(this);
        BinaryOperatorBuilder binaryOperatorBuilder = new BinaryOperatorBuilder();
        BinaryOperationType operation = ctx.muldivop().accept(binaryOperatorBuilder);
        return new BinaryOperationExpression(left, right, operation);
    }

    @Override
    public Expression visitAddSubExpr(AddSubExprContext ctx) {
        Expression left = ctx.expr(0).accept(this);
        Expression right = ctx.expr(1).accept(this);
        BinaryOperatorBuilder binaryOperatorBuilder = new BinaryOperatorBuilder();
        BinaryOperationType operation = ctx.addsubop().accept(binaryOperatorBuilder);
        return new BinaryOperationExpression(left, right, operation);
    }

    @Override
    public Expression visitInt(IntContext ctx) {
        return new IntegerExpression(Integer.parseInt(ctx.INT().getText()));
    }

    @Override
    public Expression visitDouble(DoubleContext ctx) {
        return new DoubleExpression(Double.parseDouble(ctx.DOUBLE().getText()));
    }

    @Override
    public Expression visitString(StringContext ctx) {
        return new StringExpression(ctx.STRING().getText());
    }

    @Override
    public Expression visitId(GribScriptParser.IdContext ctx) {
        String name = ctx.ID().getText();
        if (name.equals("true")) {
            return new BooleanExpression(true);
        }
        if (name.equals("false")) {
            return new BooleanExpression(false);
        }
        return new VariableExpression(ctx.ID().getText());
    }

    @Override
    public Expression visitFunction(GribScriptParser.FunctionContext ctx) {
        List<Expression> args = new ArrayList<>();
        if (ctx.functionargs() != null) {
            args = ctx.functionargs().expr().stream().map(e -> e.accept(this)).collect(Collectors.toList());
        }
        return new Function(ctx.ID().getText(), args);
    }

    @Override
    public Expression visitParens(GribScriptParser.ParensContext ctx) {
        return ctx.expr().accept(this);
    }

    @Override
    public Expression visitUnaryExpr(UnaryExprContext ctx) {
        return new UnaryOperationExpression(ctx.expr().accept(this), UnaryOperationType.NOT);
    }
}