package grib_script.ast.builder;

import grib_script.antlr4.GribScriptBaseVisitor;
import grib_script.antlr4.GribScriptParser.AddContext;
import grib_script.antlr4.GribScriptParser.AndContext;
import grib_script.antlr4.GribScriptParser.DivContext;
import grib_script.antlr4.GribScriptParser.EqContext;
import grib_script.antlr4.GribScriptParser.GtContext;
import grib_script.antlr4.GribScriptParser.GteContext;
import grib_script.antlr4.GribScriptParser.LtContext;
import grib_script.antlr4.GribScriptParser.LteContext;
import grib_script.antlr4.GribScriptParser.MulContext;
import grib_script.antlr4.GribScriptParser.NeqContext;
import grib_script.antlr4.GribScriptParser.OrContext;
import grib_script.antlr4.GribScriptParser.SubContext;
import grib_script.ast.node.BinaryOperationType;

public class BinaryOperatorBuilder extends GribScriptBaseVisitor<BinaryOperationType> {
    @Override
    public BinaryOperationType visitAnd(AndContext ctx) {
        return BinaryOperationType.AND;
    }

    @Override
    public BinaryOperationType visitOr(OrContext ctx) {
        return BinaryOperationType.OR;
    }

    @Override
    public BinaryOperationType visitEq(EqContext ctx) {
        return BinaryOperationType.EQ;
    }

    @Override
    public BinaryOperationType visitNeq(NeqContext ctx) {
        return BinaryOperationType.NEQ;
    }

    @Override
    public BinaryOperationType visitLt(LtContext ctx) {
        return BinaryOperationType.LT;
    }

    @Override
    public BinaryOperationType visitLte(LteContext ctx) {
        return BinaryOperationType.LTE;
    }

    @Override
    public BinaryOperationType visitGt(GtContext ctx) {
        return BinaryOperationType.GT;
    }

    @Override
    public BinaryOperationType visitGte(GteContext ctx) {
        return BinaryOperationType.GTE;
    }

    @Override
    public BinaryOperationType visitAdd(AddContext ctx) {
        return BinaryOperationType.SUM;
    }

    @Override
    public BinaryOperationType visitSub(SubContext ctx) {
        return BinaryOperationType.SUB;
    }

    @Override
    public BinaryOperationType visitMul(MulContext ctx) {
        return BinaryOperationType.MUL;
    }

    @Override
    public BinaryOperationType visitDiv(DivContext ctx) {
        return BinaryOperationType.DIV;
    }
}
