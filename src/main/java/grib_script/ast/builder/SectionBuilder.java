package grib_script.ast.builder;

import grib_script.antlr4.GribScriptBaseVisitor;
import grib_script.antlr4.GribScriptParser.BeginSectionContext;
import grib_script.antlr4.GribScriptParser.EndSectionContext;
import grib_script.antlr4.GribScriptParser.MatchSectionContext;
import grib_script.antlr4.GribScriptParser.StatementContext;
import grib_script.ast.node.*;

/**
 * <p>
 * Genera un sotto-AST, relativo a {@link Section}, a partire da:
 * </p>
 * <ul>
 * <li>{@link BeginSectionContext}</li>
 * <li>{@link EndSectionContext}</li>
 * <li>{@link MatchSectionContext}</li>
 * </ul>
 * <p>
 * I nodi generati implementano l'interfaccia {@link Section}.
 * </p>
 * <p>
 * TODO: gestione ValueSectionContext.
 * </p>
 */
public class SectionBuilder extends GribScriptBaseVisitor<Section> {
    @Override
    public Section visitBeginSection(BeginSectionContext ctx) {
        BeginSection section = new BeginSection();
        for (StatementContext statementCtx : ctx.beginsection().statements().statement()) {
            StatementBuilder statementBuilder = new StatementBuilder();
            Statement statement = statementCtx.accept(statementBuilder);
            section.getStatements().add(statement);
        }
        return section;
    }

    @Override
    public Section visitEndSection(EndSectionContext ctx) {
        EndSection section = new EndSection();
        for (StatementContext statementCtx : ctx.endsection().statements().statement()) {
            StatementBuilder statementBuilder = new StatementBuilder();
            Statement statement = statementCtx.accept(statementBuilder);
            section.getStatements().add(statement);
        }
        return section;
    }

    @Override
    public Section visitMatchSection(MatchSectionContext ctx) {
        Expression condition = null;
        if (ctx.matchsection().expr() != null) {
            condition = ctx.matchsection().expr().accept(new ExpressionBuilder());
        } else {
            condition = new BooleanExpression(true);
        }
        MatchSection section = new MatchSection(condition);
        for (StatementContext statementCtx : ctx.matchsection().statements().statement()) {
            StatementBuilder statementBuilder = new StatementBuilder();
            Statement statement = statementCtx.accept(statementBuilder);
            section.getStatements().add(statement);
        }
        return section;
    }
}
