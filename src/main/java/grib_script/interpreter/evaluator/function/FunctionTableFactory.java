package grib_script.interpreter.evaluator.function;

import java.util.HashMap;

public class FunctionTableFactory {
    public static HashMap<String, FunctionEvaluator> create() {
        HashMap<String, FunctionEvaluator> table = new HashMap<>();
        table.put("int", new IntFunction());
        table.put("double", new DoubleFunction());
        table.put("write", new WriteFunction());
        table.put("writegrib", new WriteGribFunction());
        table.put("getgrib", new GribGetFunction());
        table.put("setgrib", new GribSetFunction());
        table.put("print", new PrintFunction());
        return table;
    }
}
