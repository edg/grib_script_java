package grib_script.interpreter.evaluator.function;

public class FunctionEvaluatorException extends Exception {
    private static final long serialVersionUID = 1L;

    public FunctionEvaluatorException(Throwable e) {
        super(e);
    }

    public FunctionEvaluatorException(String msg) {
        super(msg);
    }
}
