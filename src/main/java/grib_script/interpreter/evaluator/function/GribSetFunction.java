package grib_script.interpreter.evaluator.function;

import grib_script.eccodes.EccodesException;
import grib_script.eccodes.GribHandle;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.GribKeyValue;
import grib_script.interpreter.evaluator.value.GribValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;

/**
 * <pre>
 * {@code
 * setgrib(grib:grib, key:string, value:any)
 * }
 * </pre>
 */
public class GribSetFunction implements FunctionEvaluator {

    @Override
    public Value evaluate(FunctionArguments arguments) throws FunctionEvaluatorException {
        try {
            GribValue gribValue = GribValue.class.cast(arguments.get(0));
            String key = arguments.get(1).toStringValue().getValue();
            Value value = arguments.get(2);
            GribHandle gribHandle = gribValue.getGribHandle();
            if (value instanceof IntegerValue) {
                gribHandle.setLongValue(key, value.toIntegerValue().getValue().longValue());
            } else if (value instanceof DoubleValue) {
                gribHandle.setDoubleValue(key, value.toDoubleValue().getValue());
            } else if (value instanceof StringValue) {
                gribHandle.setStringValue(key, value.toStringValue().getValue());
            } else if (value instanceof BooleanValue) {
                gribHandle.setLongValue(key, value.toIntegerValue().getValue().longValue());
            } else if (value instanceof GribKeyValue) {
                gribHandle.setLongValue(key, value.toIntegerValue().getValue().longValue());
            } else {
                throw new FunctionEvaluatorException("Invalid value type " + value.toString());
            }
            return new BooleanValue(true);
        } catch (ClassCastException | EccodesException e) {
            throw new FunctionEvaluatorException(e);
        }
    }

}
