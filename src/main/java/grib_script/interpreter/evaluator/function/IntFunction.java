package grib_script.interpreter.evaluator.function;

import grib_script.interpreter.evaluator.value.Value;

/**
 * <pre>
 * {@code
 * int(arg:any) -> integer
 * }
 * </pre>
 *
 */
public class IntFunction implements FunctionEvaluator {

    @Override
    public Value evaluate(FunctionArguments arguments) throws FunctionEvaluatorException {
        Value v = arguments.get(0);
        return v.toIntegerValue();
    }
}