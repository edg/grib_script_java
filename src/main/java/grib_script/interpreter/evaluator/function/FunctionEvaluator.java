package grib_script.interpreter.evaluator.function;

import grib_script.interpreter.evaluator.value.Value;

public interface FunctionEvaluator {
    public Value evaluate(FunctionArguments arguments) throws FunctionEvaluatorException;
}