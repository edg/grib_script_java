package grib_script.interpreter.evaluator.function;

import grib_script.eccodes.EccodesException;
import grib_script.eccodes.GribHandle;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.GribValue;
import grib_script.interpreter.evaluator.value.Value;

/**
 * <pre>
 * {@code
 * writegrib(filename:string, append:boolean, value:grib) -> boolean
 * }
 * </pre>
 *
 */
public class WriteGribFunction implements FunctionEvaluator {

    @Override
    public Value evaluate(FunctionArguments arguments) throws FunctionEvaluatorException {
        String filename = arguments.get(0).toStringValue().getValue();
        Boolean append = arguments.get(1).toBooleanValue().getValue();
        try {
            GribHandle handle = GribValue.class.cast(arguments.get(2)).getGribHandle();
            handle.write(filename, append);
            return new BooleanValue(true);
        } catch (ClassCastException | EccodesException e) {
            throw new FunctionEvaluatorException(e);
        }
    }
}
