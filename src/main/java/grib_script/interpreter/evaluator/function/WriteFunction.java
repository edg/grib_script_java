package grib_script.interpreter.evaluator.function;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.Value;

/**
 * <pre>
 * {@code
 * write(filename:string, append:boolean, value:string) -> boolean
 * }
 * </pre>
 *
 */
public class WriteFunction implements FunctionEvaluator {

    @Override
    public Value evaluate(FunctionArguments arguments) throws FunctionEvaluatorException {
        String filename = arguments.get(0).toStringValue().getValue();
        Boolean append = arguments.get(1).toBooleanValue().getValue();
        String content = arguments.get(2).toStringValue().getValue();
        try (PrintWriter writer = new PrintWriter(new FileWriter(filename, append))) {
            writer.write(content);
            return new BooleanValue(true);
        } catch (IOException e) {
            throw new FunctionEvaluatorException(e);
        }
    }

}
