package grib_script.interpreter.evaluator.function;

import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.Value;

public class PrintFunction implements FunctionEvaluator {

    @Override
    public Value evaluate(FunctionArguments arguments) throws FunctionEvaluatorException {
        String msg = arguments.get(0).toStringValue().getValue();
        System.out.println(msg);
        return new BooleanValue(true);
    }
}
