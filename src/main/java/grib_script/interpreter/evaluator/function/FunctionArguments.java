package grib_script.interpreter.evaluator.function;

import java.util.List;

import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;

public class FunctionArguments {
    private List<Value> arguments;

    public FunctionArguments(List<Value> arguments) {
        this.arguments = arguments;
    }

    public Value get(Integer index) {
        try {
            return arguments.get(index);
        } catch (IndexOutOfBoundsException e) {
            return new StringValue("");
        }
    }
}
