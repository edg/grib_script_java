package grib_script.interpreter.evaluator.function;

import grib_script.interpreter.evaluator.value.Value;

public class DoubleFunction implements FunctionEvaluator {

    @Override
    public Value evaluate(FunctionArguments arguments) throws FunctionEvaluatorException {
        return arguments.get(0).toDoubleValue();
    }
}
