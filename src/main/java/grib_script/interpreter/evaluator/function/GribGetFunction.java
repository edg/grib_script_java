package grib_script.interpreter.evaluator.function;

import grib_script.interpreter.evaluator.value.GribKeyValue;
import grib_script.interpreter.evaluator.value.GribValue;
import grib_script.interpreter.evaluator.value.Value;

/**
 * <pre>
 * {@code
 * getgrib(grib:grib, key:string) -> gribkey 
 * }
 * </pre>
 *
 */
public class GribGetFunction implements FunctionEvaluator {

    @Override
    public Value evaluate(FunctionArguments arguments) throws FunctionEvaluatorException {
        try {
            GribValue gribValue = GribValue.class.cast(arguments.get(0));
            String key = arguments.get(1).toStringValue().getValue();
            GribKeyValue result = new GribKeyValue(key, gribValue.getGribHandle());
            return result;
        } catch (ClassCastException e) {
            throw new FunctionEvaluatorException(e);
        }
    }
}
