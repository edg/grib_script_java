package grib_script.interpreter.evaluator.binaryoperation;

import grib_script.ast.node.BinaryOperationType;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.GribKeyValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;

class StringRightBinaryOperationVisitor extends RightBinaryOperationVisitor<StringValue> {

    protected StringRightBinaryOperationVisitor(BinaryOperationType operation, StringValue left) {
        super(operation, left);
    }

    @Override
    public Value visit(IntegerValue right) {
        switch (operation) {
        case SUM:
            return new StringValue(left.getValue() + right.getValue());
        case AND:
            return new BooleanValue(left.toBooleanValue().getValue() && right.toBooleanValue().getValue());
        case OR:
            return new BooleanValue(left.toBooleanValue().getValue() || right.toBooleanValue().getValue());
        default:
            return super.visit(right);
        }
    }

    @Override
    public Value visit(StringValue right) {
        switch (operation) {
        case SUM:
            return new StringValue(left.getValue() + right.getValue());
        case EQ:
            return new BooleanValue(left.getValue().compareTo(right.getValue()) == 0);
        case NEQ:
            return new BooleanValue(left.getValue().compareTo(right.getValue()) != 0);
        case LT:
            return new BooleanValue(left.getValue().compareTo(right.getValue()) < 0);
        case LTE:
            return new BooleanValue(left.getValue().compareTo(right.getValue()) <= 0);
        case GT:
            return new BooleanValue(left.getValue().compareTo(right.getValue()) > 0);
        case GTE:
            return new BooleanValue(left.getValue().compareTo(right.getValue()) >= 0);
        case AND:
            return new BooleanValue(left.toBooleanValue().getValue() && right.toBooleanValue().getValue());
        case OR:
            return new BooleanValue(left.toBooleanValue().getValue() || right.toBooleanValue().getValue());
        default:
            return super.visit(right);
        }
    }

    @Override
    public Value visit(DoubleValue right) {
        switch (operation) {
        case SUM:
            return new StringValue(left.getValue() + right.getValue());
        case AND:
            return new BooleanValue(left.toBooleanValue().getValue() && right.toBooleanValue().getValue());
        case OR:
            return new BooleanValue(left.toBooleanValue().getValue() || right.toBooleanValue().getValue());
        default:
            return super.visit(right);
        }
    }

    @Override
    public Value visit(BooleanValue right) {
        try {
            return new BinaryOperationEvaluator(operation, left.toBooleanValue(), right).eval();
        } catch (BinaryOperationException e) {
            throw new BinaryOperationEvaluatorRuntimeException(e);
        }
    }

    @Override
    public Value visit(GribKeyValue right) {
        try {
            return new BinaryOperationEvaluator(operation, left, right.toStringValue()).eval();
        } catch (BinaryOperationException e) {
            throw new BinaryOperationEvaluatorRuntimeException(e);
        }
    }

}
