package grib_script.interpreter.evaluator.binaryoperation;

import grib_script.ast.node.BinaryOperationType;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.GribKeyValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;

class BooleanRightBinaryOperationVisitor extends RightBinaryOperationVisitor<BooleanValue> {

    protected BooleanRightBinaryOperationVisitor(BinaryOperationType operation, BooleanValue left) {
        super(operation, left);
    }

    @Override
    public Value visit(IntegerValue right) {
        return right.toBooleanValue().accept(this);
    }

    @Override
    public Value visit(DoubleValue right) {
        return right.toBooleanValue().accept(this);
    }

    @Override
    public Value visit(GribKeyValue right) {
        return right.toBooleanValue().accept(this);
    }

    @Override
    public Value visit(StringValue right) {
        return right.toBooleanValue().accept(this);
    }

    @Override
    public Value visit(BooleanValue right) {
        switch (operation) {
        case EQ:
            return new BooleanValue(left.getValue() == right.getValue());
        case NEQ:
            return new BooleanValue(left.getValue() != right.getValue());
        case AND:
            return new BooleanValue(left.getValue() && right.getValue());
        case OR:
            return new BooleanValue(left.getValue() || right.getValue());
        default:
            break;
        }
        return super.visit(right);
    }
}
