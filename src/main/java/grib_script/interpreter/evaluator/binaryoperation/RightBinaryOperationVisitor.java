package grib_script.interpreter.evaluator.binaryoperation;

import grib_script.ast.node.BinaryOperationType;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.GribKeyValue;
import grib_script.interpreter.evaluator.value.GribValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;
import grib_script.interpreter.evaluator.value.ValueVisitor;

abstract class RightBinaryOperationVisitor<T extends Value> implements ValueVisitor<Value> {
    final BinaryOperationType operation;
    final T left;

    protected RightBinaryOperationVisitor(BinaryOperationType operation, T left) {
        this.operation = operation;
        this.left = left;
    }

    @Override
    public Value visit(BooleanValue right) {
        throw BinaryOperationEvaluatorRuntimeException.create(operation, left, right);
    }

    @Override
    public Value visit(DoubleValue right) {
        throw BinaryOperationEvaluatorRuntimeException.create(operation, left, right);
    }

    @Override
    public Value visit(GribKeyValue right) {
        throw BinaryOperationEvaluatorRuntimeException.create(operation, left, right);
    }

    @Override
    public Value visit(GribValue right) {
        throw BinaryOperationEvaluatorRuntimeException.create(operation, left, right);
    }

    @Override
    public Value visit(IntegerValue right) {
        throw BinaryOperationEvaluatorRuntimeException.create(operation, left, right);
    }

    @Override
    public Value visit(StringValue right) {
        throw BinaryOperationEvaluatorRuntimeException.create(operation, left, right);
    }
}
