/**
 * <p>
 * Gestione delle operazioni binarie nel valutatore.
 * </p>
 * <p>
 * L'unica classe visibile di questo package è
 * {@link grib_script.interpreter.evaluator.binaryoperation.BinaryOperationEvaluator},
 * che si occupa di valutare una espressione binaria (si veda la documentazione
 * della classe per un utilizzo).
 */
package grib_script.interpreter.evaluator.binaryoperation;