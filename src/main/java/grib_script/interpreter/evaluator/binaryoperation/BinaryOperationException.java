package grib_script.interpreter.evaluator.binaryoperation;

public class BinaryOperationException extends Exception {
    private static final long serialVersionUID = -1417912459509776571L;

    public BinaryOperationException(Throwable e) {
        super(e);
    }
}
