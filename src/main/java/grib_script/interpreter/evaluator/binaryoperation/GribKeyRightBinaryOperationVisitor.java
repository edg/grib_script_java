package grib_script.interpreter.evaluator.binaryoperation;

import grib_script.ast.node.BinaryOperationType;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.GribKeyValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;

class GribKeyRightBinaryOperationVisitor extends RightBinaryOperationVisitor<GribKeyValue> {

    protected GribKeyRightBinaryOperationVisitor(BinaryOperationType operation, GribKeyValue left) {
        super(operation, left);
    }

    @Override
    public Value visit(BooleanValue right) {
        try {
            return new BinaryOperationEvaluator(operation, left.toIntegerValue(), right).eval();
        } catch (BinaryOperationException e) {
            throw new BinaryOperationEvaluatorRuntimeException(e);
        }
    }

    @Override
    public Value visit(DoubleValue right) {
        try {
            return new BinaryOperationEvaluator(operation, left.toDoubleValue(), right).eval();
        } catch (BinaryOperationException e) {
            throw new BinaryOperationEvaluatorRuntimeException(e);
        }
    }

    @Override
    public Value visit(GribKeyValue right) {
        try {
            return new BinaryOperationEvaluator(operation, left.toIntegerValue(), right.toIntegerValue()).eval();
        } catch (BinaryOperationException e) {
            throw new BinaryOperationEvaluatorRuntimeException(e);
        }
    }

    @Override
    public Value visit(IntegerValue right) {
        try {
            return new BinaryOperationEvaluator(operation, left.toIntegerValue(), right).eval();
        } catch (BinaryOperationException e) {
            throw new BinaryOperationEvaluatorRuntimeException(e);
        }
    }

    @Override
    public Value visit(StringValue right) {
        try {
            return new BinaryOperationEvaluator(operation, left.toStringValue(), right).eval();
        } catch (BinaryOperationException e) {
            throw new BinaryOperationEvaluatorRuntimeException(e);
        }
    }
}
