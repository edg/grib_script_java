package grib_script.interpreter.evaluator.binaryoperation;

import grib_script.ast.node.BinaryOperationType;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.GribKeyValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;

class IntegerRightBinaryOperationVisitor extends RightBinaryOperationVisitor<IntegerValue> {

    protected IntegerRightBinaryOperationVisitor(BinaryOperationType operation, IntegerValue left) {
        super(operation, left);
    }

    @Override
    public Value visit(IntegerValue right) {
        switch (operation) {
        case SUM:
            return new IntegerValue(left.getValue().intValue() + right.getValue().intValue());
        case SUB:
            return new IntegerValue(left.getValue().intValue() - right.getValue().intValue());
        case MUL:
            return new IntegerValue(left.getValue().intValue() * right.getValue().intValue());
        case DIV:
            return new IntegerValue(left.getValue().intValue() / right.getValue().intValue());
        case EQ:
            return new BooleanValue(left.getValue().intValue() == right.getValue().intValue());
        case NEQ:
            return new BooleanValue(left.getValue().intValue() != right.getValue().intValue());
        case LT:
            return new BooleanValue(left.getValue().intValue() < right.getValue().intValue());
        case LTE:
            return new BooleanValue(left.getValue().intValue() <= right.getValue().intValue());
        case GT:
            return new BooleanValue(left.getValue().intValue() > right.getValue().intValue());
        case GTE:
            return new BooleanValue(left.getValue().intValue() >= right.getValue().intValue());
        case AND:
            return new BooleanValue(left.toBooleanValue().getValue() && right.toBooleanValue().getValue());
        case OR:
            return new BooleanValue(left.toBooleanValue().getValue() || right.toBooleanValue().getValue());
        default:
            return super.visit(right);
        }
    }

    @Override
    public Value visit(DoubleValue right) {
        switch (operation) {
        case SUM:
            return new DoubleValue(left.getValue() + right.getValue());
        case SUB:
            return new DoubleValue(left.getValue() - right.getValue());
        case MUL:
            return new DoubleValue(left.getValue() * right.getValue());
        case DIV:
            return new DoubleValue(left.getValue() / right.getValue());
        case EQ:
            return new BooleanValue(left.getValue().doubleValue() == right.getValue().doubleValue());
        case NEQ:
            return new BooleanValue(left.getValue().doubleValue() != right.getValue().doubleValue());
        case LT:
            return new BooleanValue(left.getValue() < right.getValue());
        case LTE:
            return new BooleanValue(left.getValue() <= right.getValue());
        case GT:
            return new BooleanValue(left.getValue() > right.getValue());
        case GTE:
            return new BooleanValue(left.getValue() >= right.getValue());
        case AND:
            return new BooleanValue(left.toBooleanValue().getValue() && right.toBooleanValue().getValue());
        case OR:
            return new BooleanValue(left.toBooleanValue().getValue() || right.toBooleanValue().getValue());
        default:
            return super.visit(right);
        }
    }

    @Override
    public Value visit(StringValue right) {
        switch (operation) {
        case AND:
            return new BooleanValue(left.toBooleanValue().getValue() && right.toBooleanValue().getValue());
        case OR:
            return new BooleanValue(left.toBooleanValue().getValue() || right.toBooleanValue().getValue());
        default:
            return super.visit(right);
        }
    }

    @Override
    public Value visit(BooleanValue right) {
        try {
            return new BinaryOperationEvaluator(operation, left.toBooleanValue(), right).eval();
        } catch (BinaryOperationException e) {
            throw new BinaryOperationEvaluatorRuntimeException(e);
        }
    }

    @Override
    public Value visit(GribKeyValue right) {
        try {
            return new BinaryOperationEvaluator(operation, left, right.toIntegerValue()).eval();
        } catch (BinaryOperationException e) {
            throw new BinaryOperationEvaluatorRuntimeException(e);
        }
    }
}
