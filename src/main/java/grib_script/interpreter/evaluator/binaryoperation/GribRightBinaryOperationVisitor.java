package grib_script.interpreter.evaluator.binaryoperation;

import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import grib_script.ast.node.BinaryOperationType;
import grib_script.eccodes.EccodesException;
import grib_script.eccodes.GribHandle;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.GribKeyValue;
import grib_script.interpreter.evaluator.value.GribValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.Value;

class GribRightBinaryOperationVisitor extends RightBinaryOperationVisitor<GribValue> {

    protected GribRightBinaryOperationVisitor(BinaryOperationType operation, GribValue left) {
        super(operation, left);
    }

    @Override
    public Value visit(IntegerValue right) {
        BiFunction<Double, Integer, Double> operationFunction;

        switch (operation) {
        case SUM:
            operationFunction = (l, r) -> l + r;
            break;
        case SUB:
            operationFunction = (l, r) -> l - r;
            break;
        case MUL:
            operationFunction = (l, r) -> l * r;
            break;
        case DIV:
            operationFunction = (l, r) -> l / r;
            break;
        default:
            return super.visit(right);
        }

        try {
            Double missingValue = left.getGribHandle().getMissingValue();

            return new GribValue(left.getGribHandle()
                    .cloneWithData(left.getGribHandle().getData().stream()
                            .map(v -> v.equals(missingValue) ? v : operationFunction.apply(v, right.getValue()))
                            .collect(Collectors.toList())));
        } catch (EccodesException e) {
            throw new BinaryOperationEvaluatorRuntimeException(e);
        }
    }

    @Override
    public Value visit(DoubleValue right) {
        BiFunction<Double, Double, Double> operationFunction;

        switch (operation) {
        case SUM:
            operationFunction = (l, r) -> l + r;
            break;
        case SUB:
            operationFunction = (l, r) -> l - r;
            break;
        case MUL:
            operationFunction = (l, r) -> l * r;
            break;
        case DIV:
            operationFunction = (l, r) -> l / r;
            break;
        default:
            return super.visit(right);
        }

        try {
            Double missingValue = left.getGribHandle().getMissingValue();

            return new GribValue(left.getGribHandle()
                    .cloneWithData(left.getGribHandle().getData().stream()
                            .map(v -> v.equals(missingValue) ? v : operationFunction.apply(v, right.getValue()))
                            .collect(Collectors.toList())));
        } catch (EccodesException e) {
            throw new BinaryOperationEvaluatorRuntimeException(e);
        }
    }

    @Override
    public Value visit(GribKeyValue right) {
        return right.toDoubleValue().accept(this);
    }

    @Override
    public Value visit(GribValue right) {
        BiFunction<Double, Double, Double> operationFunction;

        switch (operation) {
        case SUM:
            operationFunction = (l, r) -> l + r;
            break;
        case SUB:
            operationFunction = (l, r) -> l - r;
            break;
        case MUL:
            operationFunction = (l, r) -> l * r;
            break;
        case DIV:
            operationFunction = (l, r) -> l / r;
            break;
        default:
            return super.visit(right);
        }

        try {
            Double missingValue = left.getGribHandle().getMissingValue();
            List<Double> leftData = left.getGribHandle().getData();
            List<Double> rightData = right.getGribHandle().getData();

            if (!left.getGribHandle().getLongValue("Ni").equals(right.getGribHandle().getLongValue("Ni"))) {
                throw new BinaryOperationEvaluatorRuntimeException("GRIB messages have incompatible shape (Ni)");
            }

            if (!left.getGribHandle().getLongValue("Nj").equals(right.getGribHandle().getLongValue("Nj"))) {
                throw new BinaryOperationEvaluatorRuntimeException("GRIB messages have incompatible shape (Nj)");
            }

            GribHandle handle = left.getGribHandle()
                    .cloneWithData(IntStream.range(0, left.getGribHandle().getData().size()).mapToDouble(index -> {
                        Double leftValue = leftData.get(index);
                        Double rightValue = rightData.get(index);
                        if (leftValue.equals(missingValue)) {
                            return missingValue;
                        }
                        if (rightValue.equals(missingValue)) {
                            return leftValue;
                        }
                        return operationFunction.apply(leftValue, rightValue);
                    }).boxed().collect(Collectors.toList()));
            return new GribValue(handle);
        } catch (EccodesException e) {
            throw new BinaryOperationEvaluatorRuntimeException(e);
        }

    }
}
