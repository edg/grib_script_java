package grib_script.interpreter.evaluator.binaryoperation;

import grib_script.ast.node.BinaryOperationType;
import grib_script.interpreter.evaluator.value.Value;

/**
 * <p>
 * Oggetto che permette la valutazione di una espressione binaria. Poiché la
 * valutazione dipende dalla classe concreta dei due operandi, si deve
 * utilizzare il double dispath due volte: la prima volta, per intercettare il
 * tipo concreto dell'operando sinistro e la seconda volta per intercettare
 * quello dell'operando destro.
 * </p>
 * <p>
 * Il doppio double dispatch è realizzato usando prima il visitor pattern
 * sull'operando di sinistra ({@link LeftBinaryOperationVisitor}, il quale a sua
 * volta invoca un secondo visitor (estensione di
 * {@link RightBinaryOperationVisitor}), il quale infine può effettuare
 * l'operazione binaria tra le due classi concrete di {@link Value}.
 * </p>
 * 
 * <pre>
 * {
 *     BinaryOperationType operation = BinaryOperationType.SUM;
 *     Value left = new IntegerValue(1);
 *     Value right = new DoubleValue(4.5);
 *     BinaryOperationEvaluator evaluator = new BinaryOperationEvaluator(operation, left, right);
 *     Value result = evaluator.evaluate();
 * }
 * </pre>
 * 
 * <p>
 * Viene usata la tecnica dell'exception tunneling per gestire eccezioni che
 * possono essere lanciate all'interno dei metodi <code>visit()</code> del
 * package. Le eccezioni (tutte dichiarate internamente al package e visibili
 * solo al suo interno) sono intercettate nel metodo {@link #eval()}, che lancia
 * poi una checked exception {@link BinaryOperationException}.
 * </p>
 * 
 * @author Emanuele Di Giacomo &lt;emanuele@digiacomo.cc&gt;
 *
 */
public class BinaryOperationEvaluator {
    BinaryOperationType operation;
    Value left;
    Value right;

    public BinaryOperationEvaluator(BinaryOperationType operation, Value left, Value right) {
        this.operation = operation;
        this.left = left;
        this.right = right;
    }

    public Value eval() throws BinaryOperationException {
        try {
            return left.accept(new LeftBinaryOperationVisitor(operation, right));
        } catch (BinaryOperationEvaluatorRuntimeException e) {
            throw new BinaryOperationException(e);
        }
    }
}
