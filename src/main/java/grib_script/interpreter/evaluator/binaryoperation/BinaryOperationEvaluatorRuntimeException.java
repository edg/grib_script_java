package grib_script.interpreter.evaluator.binaryoperation;

import grib_script.ast.node.BinaryOperationType;
import grib_script.interpreter.evaluator.value.Value;

class BinaryOperationEvaluatorRuntimeException extends RuntimeException {
    private static final long serialVersionUID = -1417912459509776571L;

    public BinaryOperationEvaluatorRuntimeException(String msg) {
        super(msg);
    }

    public BinaryOperationEvaluatorRuntimeException(Throwable e) {
        super(e);
    }

    public static BinaryOperationEvaluatorRuntimeException create(BinaryOperationType operation, Value left,
            Value right) {
        return new BinaryOperationEvaluatorRuntimeException(
                String.format("Invalid operation %s between %s and %s", operation, left, right));
    }
}
