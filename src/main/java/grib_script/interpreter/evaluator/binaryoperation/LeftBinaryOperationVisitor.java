package grib_script.interpreter.evaluator.binaryoperation;

import grib_script.ast.node.BinaryOperationType;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.GribKeyValue;
import grib_script.interpreter.evaluator.value.GribValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;
import grib_script.interpreter.evaluator.value.ValueVisitor;

class LeftBinaryOperationVisitor implements ValueVisitor<Value> {
    BinaryOperationType operation;
    Value right;

    public LeftBinaryOperationVisitor(BinaryOperationType operation, Value right) {
        this.operation = operation;
        this.right = right;
    }

    @Override
    public Value visit(BooleanValue left) {
        return right.accept(new BooleanRightBinaryOperationVisitor(operation, left));
    }

    @Override
    public Value visit(DoubleValue left) {
        return right.accept(new DoubleRightBinaryOperationVisitor(operation, left));
    }

    @Override
    public Value visit(GribKeyValue left) {
        return right.accept(new GribKeyRightBinaryOperationVisitor(operation, left));
    }

    @Override
    public Value visit(GribValue left) {
        return right.accept(new GribRightBinaryOperationVisitor(operation, left));
    }

    @Override
    public Value visit(IntegerValue left) {
        return right.accept(new IntegerRightBinaryOperationVisitor(operation, left));
    }

    @Override
    public Value visit(StringValue left) {
        return right.accept(new StringRightBinaryOperationVisitor(operation, left));
    }
}
