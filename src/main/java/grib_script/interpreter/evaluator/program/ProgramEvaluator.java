package grib_script.interpreter.evaluator.program;

import java.util.List;
import java.util.stream.Collectors;

import grib_script.ast.node.BeginSection;
import grib_script.ast.node.EndSection;
import grib_script.ast.node.MatchSection;
import grib_script.ast.node.Program;
import grib_script.ast.node.Section;
import grib_script.ast.node.Statement;
import grib_script.eccodes.Grib;
import grib_script.eccodes.GribHandle;
import grib_script.interpreter.evaluator.condition.ConditionEvaluator;
import grib_script.interpreter.evaluator.context.Context;
import grib_script.interpreter.evaluator.statement.StatementEvaluationException;
import grib_script.interpreter.evaluator.statement.StatementEvaluator;
import grib_script.interpreter.evaluator.value.GribValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;

public class ProgramEvaluator {
    Context context = new Context();

    public void setVariable(String name, String value) {
        context.setVariable(name, new StringValue(value));
    }

    // public void eval(String filename, String gribFilename) throws
    // ProgramEvaluationException {
    public void eval(Program program, Grib grib) throws ProgramEvaluationException {
        try {
            evalSections(
                    program.getSections().stream().filter(s -> s instanceof BeginSection).collect(Collectors.toList()));

            int gribCount = 0;
            for (GribHandle handle : grib.gribHandleIterator()) {
                gribCount += 1;
                GribValue gribValue = new GribValue(handle);
                context.setVariable("CURRENT_GRIB", gribValue);
                context.setVariable("GRIB_COUNT", new IntegerValue(gribCount));
                for (Section section : program.getSections().stream().filter(s -> s instanceof MatchSection)
                        .collect(Collectors.toList())) {
                    MatchSection matchSection = (MatchSection) section;
                    ConditionEvaluator evaluator = new ConditionEvaluator(handle);
                    Boolean condition = evaluator.eval(matchSection.getCondition()).toBooleanValue().getValue();
                    if (condition) {
                        evalSection(matchSection);
                    }
                }
            }
            evalSections(
                    program.getSections().stream().filter(s -> s instanceof EndSection).collect(Collectors.toList()));
        } catch (Exception e) {
            throw new ProgramEvaluationException(e);
        }
    }

    private void evalSections(List<Section> sections) throws StatementEvaluationException {
        for (Section section : sections) {
            evalSection(section);
        }
    }

    private void evalSection(Section section) throws StatementEvaluationException {
        StatementEvaluator evaluator = new StatementEvaluator(context);
        for (Statement statement : section.getStatements()) {
            evaluator.eval(statement);
        }
    }
}