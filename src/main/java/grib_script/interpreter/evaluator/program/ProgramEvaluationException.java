package grib_script.interpreter.evaluator.program;

public class ProgramEvaluationException extends Exception {
    private static final long serialVersionUID = 1L;

    public ProgramEvaluationException(Throwable e) {
        super(e);
    }
}
