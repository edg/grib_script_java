package grib_script.interpreter.evaluator;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import grib_script.ast.node.Program;
import grib_script.eccodes.EccodesException;
import grib_script.eccodes.Grib;
import grib_script.interpreter.evaluator.program.ProgramEvaluationException;
import grib_script.interpreter.evaluator.program.ProgramEvaluator;
import grib_script.utils.StringParser;

public class Main {
    private Options options = new Options();

    Main() {
        Option help = Option.builder("h").hasArg(false).desc("Show this help and exit").longOpt("help").build();

        Option assign = Option.builder("v").argName("var=val").valueSeparator().numberOfArgs(2)
                .desc("Assign the value val to the variable var").longOpt("assign").build();

        Option scriptFile = Option.builder("f").argName("FILE").numberOfArgs(1).desc("Read script from FILE")
                .longOpt("file").build();

        options.addOption(help);
        options.addOption(assign);
        options.addOption(scriptFile);
    }

    void showHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("grib_script [OPTIONS] [SCRIPT] GRIB", options);
    }

    void run(String[] args) {
        try {
            CommandLineParser cliParser = new DefaultParser();
            CommandLine line = cliParser.parse(options, args);

            if (line.hasOption("h")) {
                showHelp();
                System.exit(0);
            }

            if ((line.hasOption("f") && line.getArgList().size() != 1)
                    || (!line.hasOption("f") && line.getArgList().size() != 2)) {
                showHelp();
                System.exit(1);
            }

            StringParser gribScriptParser;
            String gribFilename;
            if (line.hasOption("f")) {
                String scriptFilename = line.getOptionValue("f");
                gribFilename = line.getArgList().get(0);
                gribScriptParser = StringParser.fromFile(scriptFilename);
            } else {
                String scriptString = line.getArgList().get(0);
                gribFilename = line.getArgList().get(1);
                gribScriptParser = StringParser.fromString(scriptString);
            }

            Program program = gribScriptParser.parse();

            Grib grib = new Grib();
            grib.open(gribFilename);

            ProgramEvaluator evaluator = new ProgramEvaluator();
            Properties assignment = line.getOptionProperties("v");
            assignment.forEach((name, value) -> evaluator.setVariable((String) name, (String) value));

            evaluator.eval(program, grib);
        } catch (ParseException e) {
            showHelp();
            System.exit(1);
        } catch (ProgramEvaluationException | IOException | EccodesException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static void main(String[] args) {
        new Main().run(args);
    }
}
