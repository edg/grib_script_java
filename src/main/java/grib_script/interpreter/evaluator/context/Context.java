package grib_script.interpreter.evaluator.context;

import grib_script.interpreter.evaluator.function.FunctionEvaluator;
import grib_script.interpreter.evaluator.value.Value;

public class Context {
    private VariableTable variableTable = new VariableTable();
    private FunctionTable functionTable = new FunctionTable();

    public Value getVariableValue(String name) {
        return variableTable.getVariableValue(name);
    }

    public void setVariable(String name, Value value) {
        variableTable.setVariable(name, value);
    }

    public FunctionEvaluator getFunction(String name) throws FunctionNotFoundException {
        return functionTable.getFunction(name);
    }
}
