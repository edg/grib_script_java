package grib_script.interpreter.evaluator.context;

import java.util.HashMap;

import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;

class VariableTable {
    private HashMap<String, Value> table = new HashMap<>();

    public VariableTable() {
    }

    public Value getVariableValue(String name) {
        if (table.containsKey(name)) {
            return table.get(name);
        } else {
            return new StringValue("");
        }
    }

    public void setVariable(String name, Value value) {
        table.put(name, value);
    }
}