package grib_script.interpreter.evaluator.context;

import java.util.HashMap;

import grib_script.interpreter.evaluator.function.FunctionEvaluator;
import grib_script.interpreter.evaluator.function.FunctionTableFactory;

class FunctionTable {
    private HashMap<String, FunctionEvaluator> table = FunctionTableFactory.create();

    public FunctionEvaluator getFunction(String name) throws FunctionNotFoundException {
        if (table.containsKey(name)) {
            return table.get(name);
        } else {
            throw new FunctionNotFoundException(name);
        }
    }
}