package grib_script.interpreter.evaluator.context;

public class FunctionNotFoundException extends Exception {
    private static final long serialVersionUID = 1L;

    public FunctionNotFoundException(String name) {
        super("Function not found: " + name);
    }
}
