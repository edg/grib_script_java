package grib_script.interpreter.evaluator.expression;

class ExpressionEvaluatorRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ExpressionEvaluatorRuntimeException(Throwable e) {
        super(e);
    }
}
