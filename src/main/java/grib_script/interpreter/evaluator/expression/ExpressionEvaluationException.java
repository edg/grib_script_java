package grib_script.interpreter.evaluator.expression;

public class ExpressionEvaluationException extends Exception {
    private static final long serialVersionUID = 1L;

    public ExpressionEvaluationException(Throwable e) {
        super(e);
    }
}
