package grib_script.interpreter.evaluator.expression;

import grib_script.ast.node.Expression;
import grib_script.interpreter.evaluator.context.Context;
import grib_script.interpreter.evaluator.value.Value;

public class ExpressionEvaluator {
    private Context context;

    public ExpressionEvaluator(Context context) {
        this.context = context;
    }

    public Value eval(Expression expression) throws ExpressionEvaluationException {
        try {
            return expression.accept(new ExpressionEvaluatorVisitor(context));
        } catch (ExpressionEvaluatorRuntimeException e) {
            throw new ExpressionEvaluationException(e);
        }
    }
}
