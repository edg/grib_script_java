package grib_script.interpreter.evaluator.expression;

import java.util.stream.Collectors;

import grib_script.ast.node.BinaryOperationExpression;
import grib_script.ast.node.BinaryOperationType;
import grib_script.ast.node.BooleanExpression;
import grib_script.ast.node.DoubleExpression;
import grib_script.ast.node.Function;
import grib_script.ast.node.IntegerExpression;
import grib_script.ast.node.StringExpression;
import grib_script.ast.node.UnaryOperationExpression;
import grib_script.ast.node.VariableExpression;
import grib_script.ast.visitor.ExpressionVisitor;
import grib_script.interpreter.evaluator.binaryoperation.BinaryOperationEvaluator;
import grib_script.interpreter.evaluator.binaryoperation.BinaryOperationException;
import grib_script.interpreter.evaluator.context.Context;
import grib_script.interpreter.evaluator.context.FunctionNotFoundException;
import grib_script.interpreter.evaluator.function.FunctionArguments;
import grib_script.interpreter.evaluator.function.FunctionEvaluator;
import grib_script.interpreter.evaluator.function.FunctionEvaluatorException;
import grib_script.interpreter.evaluator.unaryoperation.UnaryOperationEvaluator;
import grib_script.interpreter.evaluator.unaryoperation.UnaryOperationException;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;

class ExpressionEvaluatorVisitor implements ExpressionVisitor<Value> {
    private Context context;

    public ExpressionEvaluatorVisitor(Context context) {
        this.context = context;
    }

    @Override
    public Value visit(BinaryOperationExpression expression) {
        BinaryOperationType operation = expression.getOperation();
        Value left = expression.getLeft().accept(this);
        Value right = expression.getRight().accept(this);
        try {
            return new BinaryOperationEvaluator(operation, left, right).eval();
        } catch (BinaryOperationException e) {
            throw new ExpressionEvaluatorRuntimeException(e);
        }
    }

    @Override
    public Value visit(BooleanExpression expression) {
        return new BooleanValue(expression);
    }

    @Override
    public Value visit(DoubleExpression expression) {
        return new DoubleValue(expression);
    }

    @Override
    public Value visit(Function expression) {
        FunctionEvaluator function;
        try {
            function = context.getFunction(expression.getName());
        } catch (FunctionNotFoundException e) {
            throw new ExpressionEvaluatorRuntimeException(e);
        }
        FunctionArguments arguments = new FunctionArguments(
                expression.getArgs().stream().map(e -> e.accept(this)).collect(Collectors.toList()));
        try {
            return function.evaluate(arguments);
        } catch (FunctionEvaluatorException e) {
            throw new ExpressionEvaluatorRuntimeException(e);
        }
    }

    @Override
    public Value visit(IntegerExpression expression) {
        return new IntegerValue(expression);
    }

    @Override
    public Value visit(StringExpression expression) {
        return new StringValue(expression);
    }

    @Override
    public Value visit(VariableExpression expression) {
        String key = expression.getName();
        Value value = context.getVariableValue(key);
        if (value == null) {
            value = new StringValue("");
            context.setVariable(key, value);
        }
        return value;
    }

    @Override
    public Value visit(UnaryOperationExpression unaryOperationExpression) {
        Value value = unaryOperationExpression.getExpression().accept(this);
        try {
            return new UnaryOperationEvaluator(unaryOperationExpression.getOperationType(), value).eval();
        } catch (UnaryOperationException e) {
            throw new ExpressionEvaluatorRuntimeException(e);
        }
    }
}
