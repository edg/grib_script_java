package grib_script.interpreter.evaluator.unaryoperation;

public class UnaryOperationException extends Exception {

    public UnaryOperationException(Throwable e) {
        super(e);
    }

}
