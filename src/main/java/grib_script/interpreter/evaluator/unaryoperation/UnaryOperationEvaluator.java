package grib_script.interpreter.evaluator.unaryoperation;

import grib_script.ast.node.UnaryOperationType;
import grib_script.interpreter.evaluator.value.Value;

public class UnaryOperationEvaluator {
    private final UnaryOperationType operationType;
    private final Value value;

    public UnaryOperationEvaluator(UnaryOperationType operationType, Value value) {
        super();
        this.operationType = operationType;
        this.value = value;
    }

    public Value eval() throws UnaryOperationException {
        try {
            return value.accept(new UnaryOperationEvaluatorVisitor(operationType));
        } catch (UnaryOperationEvaluatorRuntimeException e) {
            throw new UnaryOperationException(e);
        }
    }
}
