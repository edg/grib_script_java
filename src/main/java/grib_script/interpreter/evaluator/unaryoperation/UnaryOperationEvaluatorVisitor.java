package grib_script.interpreter.evaluator.unaryoperation;

import grib_script.ast.node.UnaryOperationType;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.GribKeyValue;
import grib_script.interpreter.evaluator.value.GribValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;
import grib_script.interpreter.evaluator.value.ValueVisitor;

class UnaryOperationEvaluatorVisitor implements ValueVisitor<Value> {
    UnaryOperationType operationType;

    public UnaryOperationEvaluatorVisitor(UnaryOperationType operationType) {
        this.operationType = operationType;
    }

    @Override
    public Value visit(BooleanValue booleanValue) {
        switch (operationType) {
        case NOT:
            return new BooleanValue(!booleanValue.getValue());
        default:
            break;
        }
        throw new UnaryOperationEvaluatorRuntimeException();
    }

    @Override
    public Value visit(DoubleValue doubleValue) {
        switch (operationType) {
        case NOT:
            return new BooleanValue(!doubleValue.toBooleanValue().getValue());
        default:
            break;
        }
        throw new UnaryOperationEvaluatorRuntimeException();
    }

    @Override
    public Value visit(GribKeyValue gribKeyValue) {
        switch (operationType) {
        case NOT:
            return new BooleanValue(!gribKeyValue.toBooleanValue().getValue());
        default:
            break;
        }
        throw new UnaryOperationEvaluatorRuntimeException();
    }

    @Override
    public Value visit(GribValue gribValue) {
        switch (operationType) {
        case NOT:
            return new BooleanValue(!gribValue.toBooleanValue().getValue());
        default:
            break;
        }
        throw new UnaryOperationEvaluatorRuntimeException();
    }

    @Override
    public Value visit(IntegerValue integerValue) {
        switch (operationType) {
        case NOT:
            return new BooleanValue(!integerValue.toBooleanValue().getValue());
        default:
            break;
        }
        throw new UnaryOperationEvaluatorRuntimeException();
    }

    @Override
    public Value visit(StringValue stringValue) {
        switch (operationType) {
        case NOT:
            return new BooleanValue(!stringValue.toBooleanValue().getValue());
        default:
            break;
        }
        throw new UnaryOperationEvaluatorRuntimeException();
    }
}
