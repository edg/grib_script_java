package grib_script.interpreter.evaluator.value;

import grib_script.ast.node.VariableExpression;
import grib_script.eccodes.EccodesException;
import grib_script.eccodes.GribHandle;

public class GribKeyValue implements Value {
    String key;
    GribHandle gribHandle;

    public GribKeyValue(VariableExpression expression, GribHandle gribHandle) {
        this.key = expression.getName();
        this.gribHandle = gribHandle;
    }

    public GribKeyValue(String key, GribHandle gribHandle) {
        this.key = key;
        this.gribHandle = gribHandle;
    }

    @Override
    public <T> T accept(ValueVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public StringValue toStringValue() {
        try {
            return new StringValue(gribHandle.getStringValue(key));
        } catch (EccodesException e) {
            return new StringValue("");
        }
    }

    @Override
    public IntegerValue toIntegerValue() {
        try {
            return new IntegerValue(gribHandle.getLongValue(key).intValue());
        } catch (EccodesException e) {
            return new IntegerValue(0);
        }
    }

    @Override
    public DoubleValue toDoubleValue() {
        try {
            return new DoubleValue(gribHandle.getDoubleValue(key));
        } catch (EccodesException e) {
            return new DoubleValue(0.0);
        }
    }

    @Override
    public BooleanValue toBooleanValue() {
        try {
            return new IntegerValue(gribHandle.getLongValue(key).intValue()).toBooleanValue();
        } catch (EccodesException e) {
            return new BooleanValue(false);
        }
    }
}
