package grib_script.interpreter.evaluator.value;

import grib_script.ast.node.IntegerExpression;

public class IntegerValue implements Value {
    Integer value;

    public IntegerValue(Integer value) {
        this.value = value;
    }

    public IntegerValue(IntegerExpression expression) {
        this.value = expression.getValue();
    }

    public String toString() {
        return String.format("Integer(%d)", this.value);
    }

    public Integer getValue() {
        return this.value;
    }

    @Override
    public <T> T accept(ValueVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public StringValue toStringValue() {
        return new StringValue(String.format("%d", this.value));
    }

    @Override
    public IntegerValue toIntegerValue() {
        return this;
    }

    @Override
    public DoubleValue toDoubleValue() {
        return new DoubleValue(this.value.doubleValue());
    }

    @Override
    public BooleanValue toBooleanValue() {
        return new BooleanValue(this.value != 0);
    }
}