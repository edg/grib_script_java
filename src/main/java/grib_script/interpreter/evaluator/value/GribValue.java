package grib_script.interpreter.evaluator.value;

import grib_script.eccodes.GribHandle;

public class GribValue implements Value {
    GribHandle handle;

    public GribValue(GribHandle handle) {
        this.handle = handle;
    }

    protected GribValue clone() {
        return new GribValue(handle.clone());
    }

    public String toString() {
        return "Grib";
    }

    @Override
    public <T> T accept(ValueVisitor<T> visitor) {
        return visitor.visit(this);
    }

    public GribHandle getGribHandle() {
        return handle;
    }

    @Override
    public StringValue toStringValue() {
        return new StringValue("");
    }

    @Override
    public IntegerValue toIntegerValue() {
        return new IntegerValue(0);
    }

    @Override
    public DoubleValue toDoubleValue() {
        return new DoubleValue(0.0);
    }

    @Override
    public BooleanValue toBooleanValue() {
        return new BooleanValue(false);
    }
}
