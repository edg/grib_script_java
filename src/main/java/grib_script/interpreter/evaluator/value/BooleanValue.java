package grib_script.interpreter.evaluator.value;

import grib_script.ast.node.BooleanExpression;

public class BooleanValue implements Value {
    Boolean value;

    public BooleanValue(Boolean value) {
        this.value = value;
    }

    public BooleanValue(BooleanExpression expression) {
        this.value = expression.getValue();
    }

    public Boolean getValue() {
        return this.value;
    }

    public String toString() {
        return String.format("Boolean(%s)", this.value);
    }

    @Override
    public <T> T accept(ValueVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public StringValue toStringValue() {
        return this.value ? new StringValue("1") : new StringValue("0");
    }

    @Override
    public IntegerValue toIntegerValue() {
        return this.value ? new IntegerValue(1) : new IntegerValue(1);
    }

    @Override
    public DoubleValue toDoubleValue() {
        return this.value ? new DoubleValue(1.0) : new DoubleValue(0.0);
    }

    @Override
    public BooleanValue toBooleanValue() {
        return this;
    }
}
