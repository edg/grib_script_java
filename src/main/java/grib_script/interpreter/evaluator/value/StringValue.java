package grib_script.interpreter.evaluator.value;

import grib_script.ast.node.StringExpression;

public class StringValue implements Value {
    String value;

    public StringValue(StringExpression expression) {
        this.value = expression.getUnquotedValue();
    }

    public StringValue(String value) {
        this.value = value;
    }

    public String toString() {
        return String.format("String(%s)", this.value);
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public <T> T accept(ValueVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public StringValue toStringValue() {
        return this;
    }

    @Override
    public BooleanValue toBooleanValue() {
        return new BooleanValue(this.value != "");
    }

    @Override
    public DoubleValue toDoubleValue() {
        try {
            return new DoubleValue(Double.parseDouble(this.value));
        } catch (NumberFormatException e) {
            return new DoubleValue(0.0);
        }
    }

    @Override
    public IntegerValue toIntegerValue() {
        try {
            return new IntegerValue(Integer.parseInt(this.value));
        } catch (NumberFormatException e) {
            return new IntegerValue(0);
        }
    }
}
