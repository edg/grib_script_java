package grib_script.interpreter.evaluator.value;

public interface ValueVisitor<T> {
    public T visit(BooleanValue booleanValue);

    public T visit(DoubleValue doubleValue);

    public T visit(GribKeyValue gribKeyValue);

    public T visit(GribValue gribValue);

    public T visit(IntegerValue integerValue);

    public T visit(StringValue stringValue);
}
