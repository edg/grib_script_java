package grib_script.interpreter.evaluator.value;

import grib_script.ast.node.DoubleExpression;

public class DoubleValue implements Value {
    Double value;

    public DoubleValue(Double value) {
        this.value = value;
    }

    public DoubleValue(DoubleExpression expression) {
        this.value = expression.getValue();
    }

    public String toString() {
        return String.format("Double(%s)", this.value);
    }

    public Double getValue() {
        return this.value;
    }

    @Override
    public <T> T accept(ValueVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public StringValue toStringValue() {
        return new StringValue(String.format("%f", this.value));
    }

    @Override
    public IntegerValue toIntegerValue() {
        return new IntegerValue(this.value.intValue());
    }

    @Override
    public DoubleValue toDoubleValue() {
        return this;
    }

    @Override
    public BooleanValue toBooleanValue() {
        return new BooleanValue(value != 0.0);
    }
}
