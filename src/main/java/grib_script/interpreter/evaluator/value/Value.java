package grib_script.interpreter.evaluator.value;

public interface Value {
    public <T> T accept(ValueVisitor<T> visitor);

    public StringValue toStringValue();

    public IntegerValue toIntegerValue();

    public DoubleValue toDoubleValue();

    public BooleanValue toBooleanValue();
}
