package grib_script.interpreter.evaluator.condition;

public class ConditionEvaluatorException extends Exception {
    private static final long serialVersionUID = 1L;

    public ConditionEvaluatorException(Throwable e) {
        super(e);
    }
}
