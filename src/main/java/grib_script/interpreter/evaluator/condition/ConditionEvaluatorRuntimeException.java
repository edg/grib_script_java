package grib_script.interpreter.evaluator.condition;

class ConditionEvaluatorRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ConditionEvaluatorRuntimeException(Throwable e) {
        super(e);
    }

    public ConditionEvaluatorRuntimeException(String msg) {
        super(msg);
    }
}
