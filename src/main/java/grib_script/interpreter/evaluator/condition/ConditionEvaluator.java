package grib_script.interpreter.evaluator.condition;

import grib_script.ast.node.Expression;
import grib_script.eccodes.GribHandle;
import grib_script.interpreter.evaluator.value.Value;

public class ConditionEvaluator {
    private GribHandle handle;

    public ConditionEvaluator(GribHandle handle) {
        this.handle = handle;
    }

    public Value eval(Expression expression) throws ConditionEvaluatorException {
        ConditionEvaluatorVisitor visitor = new ConditionEvaluatorVisitor(handle);
        try {
            return expression.accept(visitor);
        } catch (ConditionEvaluatorRuntimeException e) {
            throw new ConditionEvaluatorException(e);
        }
    }
}
