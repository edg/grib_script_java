package grib_script.interpreter.evaluator.condition;

import grib_script.ast.node.BinaryOperationExpression;
import grib_script.ast.node.BinaryOperationType;
import grib_script.ast.node.BooleanExpression;
import grib_script.ast.node.DoubleExpression;
import grib_script.ast.node.Function;
import grib_script.ast.node.IntegerExpression;
import grib_script.ast.node.StringExpression;
import grib_script.ast.node.UnaryOperationExpression;
import grib_script.ast.node.VariableExpression;
import grib_script.ast.visitor.ExpressionVisitor;
import grib_script.eccodes.GribHandle;
import grib_script.interpreter.evaluator.binaryoperation.BinaryOperationEvaluator;
import grib_script.interpreter.evaluator.binaryoperation.BinaryOperationException;
import grib_script.interpreter.evaluator.unaryoperation.UnaryOperationEvaluator;
import grib_script.interpreter.evaluator.unaryoperation.UnaryOperationException;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.GribKeyValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;

class ConditionEvaluatorVisitor implements ExpressionVisitor<Value> {
    private final GribHandle gribHandle;

    public ConditionEvaluatorVisitor(GribHandle gribHandle) {
        this.gribHandle = gribHandle;
    }

    @Override
    public Value visit(BinaryOperationExpression expression) {
        BinaryOperationType operation = expression.getOperation();
        Value left = expression.getLeft().accept(this);
        Value right = expression.getRight().accept(this);
        try {
            return new BinaryOperationEvaluator(operation, left, right).eval();
        } catch (BinaryOperationException e) {
            throw new ConditionEvaluatorRuntimeException(e);
        }
    }

    @Override
    public Value visit(BooleanExpression expression) {
        return new BooleanValue(expression);
    }

    @Override
    public Value visit(DoubleExpression expression) {
        return new DoubleValue(expression);
    }

    @Override
    public Value visit(Function expression) {
        throw new ConditionEvaluatorRuntimeException(
                "Use of functions is not allowed in the condition expression of a match section");
    }

    @Override
    public Value visit(IntegerExpression expression) {
        return new IntegerValue(expression);
    }

    @Override
    public Value visit(StringExpression expression) {
        return new StringValue(expression);
    }

    @Override
    public Value visit(VariableExpression expression) {
        return new GribKeyValue(expression, gribHandle);
    }

    @Override
    public Value visit(UnaryOperationExpression unaryOperationExpression) {
        Value value = unaryOperationExpression.getExpression().accept(this);
        try {
            return new UnaryOperationEvaluator(unaryOperationExpression.getOperationType(), value).eval();
        } catch (UnaryOperationException e) {
            throw new ConditionEvaluatorRuntimeException(e);
        }
    }

}
