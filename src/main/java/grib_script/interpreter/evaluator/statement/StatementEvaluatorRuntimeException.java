package grib_script.interpreter.evaluator.statement;

class StatementEvaluatorRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public StatementEvaluatorRuntimeException(Throwable e) {
        super(e);
    }
}
