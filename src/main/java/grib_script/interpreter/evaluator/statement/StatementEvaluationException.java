package grib_script.interpreter.evaluator.statement;

public class StatementEvaluationException extends Exception {
    private static final long serialVersionUID = -8082782924861824990L;

    public StatementEvaluationException(Throwable e) {
        super(e);
    }
}
