package grib_script.interpreter.evaluator.statement;

import grib_script.ast.node.Statement;
import grib_script.interpreter.evaluator.context.Context;

public class StatementEvaluator {
    private Context context;

    public StatementEvaluator(Context context) {
        this.context = context;
    }

    public void eval(Statement statement) throws StatementEvaluationException {
        try {
            StatementEvaluatorVisitor visitor = new StatementEvaluatorVisitor(context);
            statement.accept(visitor);
        } catch (StatementEvaluatorRuntimeException e) {
            throw new StatementEvaluationException(e);
        }
    }
}
