package grib_script.interpreter.evaluator.statement;

import java.util.List;
import java.util.stream.Collectors;

import grib_script.ast.node.Assignment;
import grib_script.ast.node.Expression;
import grib_script.ast.node.Function;
import grib_script.ast.node.IfBlock;
import grib_script.ast.node.IfElse;
import grib_script.ast.node.Statement;
import grib_script.ast.visitor.StatementVisitor;
import grib_script.interpreter.evaluator.context.Context;
import grib_script.interpreter.evaluator.context.FunctionNotFoundException;
import grib_script.interpreter.evaluator.expression.ExpressionEvaluationException;
import grib_script.interpreter.evaluator.expression.ExpressionEvaluator;
import grib_script.interpreter.evaluator.function.FunctionArguments;
import grib_script.interpreter.evaluator.function.FunctionEvaluator;
import grib_script.interpreter.evaluator.function.FunctionEvaluatorException;
import grib_script.interpreter.evaluator.value.Value;

class StatementEvaluatorVisitor implements StatementVisitor<Void> {
    private Context context;

    public StatementEvaluatorVisitor(Context context) {
        this.context = context;
    }

    @Override
    public Void visit(Assignment assignment) {
        String name = assignment.getName();
        ExpressionEvaluator evaluator = new ExpressionEvaluator(context);
        Value value;
        try {
            value = evaluator.eval(assignment.getExpression());
        } catch (ExpressionEvaluationException e) {
            throw new StatementEvaluatorRuntimeException(e);
        }
        context.setVariable(name, value);
        return null;
    }

    @Override
    public Void visit(Function function) {
        FunctionEvaluator functionEvaluator;
        try {
            functionEvaluator = context.getFunction(function.getName());
        } catch (FunctionNotFoundException e) {
            throw new StatementEvaluatorRuntimeException(e);
        }
        List<Value> arguments = function.getArgs().stream().map(arg -> {
            ExpressionEvaluator evaluator = new ExpressionEvaluator(context);
            Value value;
            try {
                value = evaluator.eval(arg);
            } catch (ExpressionEvaluationException e) {
                throw new StatementEvaluatorRuntimeException(e);
            }
            return value;
        }).collect(Collectors.toList());
        try {
            functionEvaluator.evaluate(new FunctionArguments(arguments));
        } catch (FunctionEvaluatorException e) {
            throw new StatementEvaluatorRuntimeException(e);
        }
        return null;
    }

    @Override
    public Void visit(IfElse ifElse) {
        for (IfBlock ifblock : ifElse.getIfBlocks()) {
            Expression condition = ifblock.getCondition();
            ExpressionEvaluator evaluator = new ExpressionEvaluator(context);
            Boolean value;
            try {
                value = evaluator.eval(condition).toBooleanValue().getValue();
            } catch (ExpressionEvaluationException e) {
                throw new StatementEvaluatorRuntimeException(e);
            }
            if (value) {
                for (Statement statement : ifblock.getStatements()) {
                    statement.accept(new StatementEvaluatorVisitor(context));
                }
                return null;
            }
        }
        return null;
    }

}
