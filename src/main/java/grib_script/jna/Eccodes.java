package grib_script.jna;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Platform;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.LongByReference;

/**
 * <p>
 * JNA binding per eccodes.
 * </p>
 * 
 * @author Emanuele Di Giacomo &lt;emanuele@digiacomo.cc&gt;
 *
 */
public class Eccodes implements Library {
    public static final CLibrary INSTANCE = Native.load((Platform.isWindows() ? "eccodes" : "eccodes"), CLibrary.class);

    public interface CLibrary extends Library {
        long codes_get_api_version();

        Pointer codes_grib_handle_new_from_file(Pointer ctx, Libc.CLibrary.FILE f, IntByReference err);

        Pointer codes_handle_clone(Pointer handle);

        int codes_handle_delete(Pointer handle);

        int codes_get_size(Pointer handle, String key, LongByReference length);

        int codes_get_length(Pointer handle, String key, LongByReference length);

        int codes_get_long(Pointer handle, String key, LongByReference value);

        int codes_get_double(Pointer handle, String key, DoubleByReference value);

        int codes_get_string(Pointer handle, String key, Pointer mesg, LongByReference length);

        int codes_get_double_array(Pointer handle, String key, Pointer vals, LongByReference length);

        int codes_set_double_array(Pointer handle, String key, Pointer vals, Long length);

        int codes_set_long(Pointer handle, String key, Long val);

        int codes_set_double(Pointer handle, String key, Double val);

        int codes_set_string(Pointer handle, String key, String mesg, LongByReference length);

        int codes_write_message(Pointer handle, String filename, String mode);
    }
}
