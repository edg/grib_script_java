package grib_script.jna;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Platform;
import com.sun.jna.Pointer;
import com.sun.jna.PointerType;

public class Libc {
    public static final CLibrary INSTANCE = Native.load((Platform.isWindows() ? "msvcrt" : "c"), CLibrary.class);

    public interface CLibrary extends Library {
        class FILE extends PointerType {
            public FILE() {
            }

            public FILE(Pointer pointer) {
                super(pointer);
            }
        }

        FILE fopen(String path, String mode);

        int fclose(FILE f);
    }
}
