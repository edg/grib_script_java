package grib_script.utils;

import grib_script.antlr4.GribScriptLexer;
import grib_script.antlr4.GribScriptParser;
import grib_script.ast.node.Expression;
import grib_script.ast.node.Program;
import grib_script.ast.builder.ExpressionBuilder;
import grib_script.ast.builder.ProgramBuilder;

import java.io.IOException;

import org.antlr.v4.runtime.*;

public class StringParser {
    private final CharStream input;

    private StringParser(CharStream input) {
        this.input = input;
    }

    public static StringParser fromFile(String filename) throws IOException {
        return new StringParser(CharStreams.fromFileName(filename));
    }

    public static StringParser fromString(String string) {
        return new StringParser(CharStreams.fromString(string));
    }

    public Program parse() {
        GribScriptParser.ProgramContext programContext = createParser().program();
        ProgramBuilder builder = new ProgramBuilder();
        return programContext.accept(builder);
    }

    public Expression parseExpression() {
        GribScriptParser.ExprContext exprContext = createParser().expr();
        ExpressionBuilder builder = new ExpressionBuilder();
        return exprContext.accept(builder);
    }

    private GribScriptParser createParser() {
        GribScriptLexer lexer = new GribScriptLexer(this.input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        GribScriptParser parser = new GribScriptParser(tokens);
        lexer.removeErrorListeners();
        lexer.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int pos, String msg,
                    RecognitionException e) {
                throw new IllegalStateException("Failed to parse at " + line + "," + pos + ":  " + msg, e);
            }
        });
        parser.removeErrorListeners();
        parser.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int pos, String msg,
                    RecognitionException e) {
                throw new IllegalStateException("Failed to parse at " + line + "," + pos + ":  " + msg, e);
            }
        });
        return parser;
    }
}
