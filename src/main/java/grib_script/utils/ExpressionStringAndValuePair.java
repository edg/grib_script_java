package grib_script.utils;

import grib_script.interpreter.evaluator.value.Value;

public class ExpressionStringAndValuePair {
    public final String inputString;
    public final Value expectedValue;

    public ExpressionStringAndValuePair(String inputString, Value expectedValue) {
        this.inputString = inputString;
        this.expectedValue = expectedValue;
    }

    public ExpressionStringAndValuePair(String inputString) {
        this.inputString = inputString;
        this.expectedValue = null;
    }

    public String toString() {
        if (expectedValue == null) {
            return String.format("%s -> error", inputString);
        } else {
            return String.format("%s -> %s", inputString, expectedValue);
        }
    }
}
