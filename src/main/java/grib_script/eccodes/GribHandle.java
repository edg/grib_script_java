package grib_script.eccodes;

import java.util.ArrayList;
import java.util.List;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.LongByReference;

import grib_script.jna.Eccodes;

public class GribHandle {
    protected Pointer handle;

    public GribHandle(Pointer handle) {
        this.handle = handle;
    }

    public GribHandle clone() {
        return new GribHandle(Eccodes.INSTANCE.codes_handle_clone(handle));
    }

    public GribHandle cloneWithData(List<Double> data) throws EccodesException {
        GribHandle h = this.clone();
        h.setData(data);
        return h;
    }

    public List<Double> getData() throws EccodesException {
        int eccodesError;
        List<Double> data;
        LongByReference length = new LongByReference();
        eccodesError = Eccodes.INSTANCE.codes_get_size(handle, "values", length);
        if (eccodesError != 0) {
            throw new EccodesException(eccodesError);
        }
        Pointer values = new Memory(Native.getNativeSize(Double.TYPE) * length.getValue());
        eccodesError = Eccodes.INSTANCE.codes_get_double_array(handle, "values", values, length);
        if (eccodesError != 0) {
            throw new EccodesException(eccodesError);
        }
        data = new ArrayList<Double>();
        for (int i = 0; i < length.getValue(); ++i) {
            double value = values.getDouble(i * Native.getNativeSize(Double.TYPE));
            data.add(value);
        }
        return data;
    }

    public void setData(List<Double> data) throws EccodesException {
        int eccodesError;
        LongByReference length = new LongByReference();
        eccodesError = Eccodes.INSTANCE.codes_get_size(handle, "values", length);
        if (eccodesError != 0) {
            throw new EccodesException(eccodesError);
        }
        Pointer values = new Memory(Native.getNativeSize(Double.TYPE) * length.getValue());
        for (int i = 0; i < length.getValue(); ++i) {
            values.setDouble(i * Native.getNativeSize(Double.TYPE), data.get(i));
        }
        eccodesError = Eccodes.INSTANCE.codes_set_double_array(handle, "values", values, length.getValue());
        if (eccodesError != 0) {
            throw new EccodesException(eccodesError);
        }
    }

    public void write(String filename, Boolean append) throws EccodesException {
        int eccodesError;
        eccodesError = Eccodes.INSTANCE.codes_write_message(handle, filename, append ? "a" : "w");
        if (eccodesError != 0) {
            throw new EccodesException(eccodesError);
        }
    }

    public Double getMissingValue() throws EccodesException {
        int eccodesError;
        DoubleByReference missingValue = new DoubleByReference();
        eccodesError = Eccodes.INSTANCE.codes_get_double(handle, "missingValue", missingValue);
        if (eccodesError != 0) {
            throw new EccodesException(eccodesError);
        }
        return missingValue.getValue();

    }

    public Long getLongValue(String key) throws EccodesException {
        int eccodesError;
        LongByReference value = new LongByReference(0);
        eccodesError = Eccodes.INSTANCE.codes_get_long(handle, key, value);
        if (eccodesError != 0) {
            throw new EccodesException(eccodesError);
        }
        return value.getValue();
    }

    public Double getDoubleValue(String key) throws EccodesException {
        int eccodesError;
        DoubleByReference value = new DoubleByReference(0);
        eccodesError = Eccodes.INSTANCE.codes_get_double(handle, key, value);
        if (eccodesError != 0) {
            throw new EccodesException(eccodesError);
        }
        return value.getValue();
    }

    public String getStringValue(String key) throws EccodesException {
        int eccodesError;
        LongByReference length = new LongByReference(1024);
        Pointer value = new Memory(length.getValue());
        eccodesError = Eccodes.INSTANCE.codes_get_string(handle, key, value, length);
        if (eccodesError != 0) {
            throw new EccodesException(eccodesError);
        }
        return value.getString(0);
    }

    public void setLongValue(String key, Long value) throws EccodesException {
        int eccodesError;
        eccodesError = Eccodes.INSTANCE.codes_set_long(handle, key, value);
        if (eccodesError != 0) {
            throw new EccodesException(eccodesError);
        }
    }

    public void setDoubleValue(String key, Double value) throws EccodesException {
        int eccodesError;
        eccodesError = Eccodes.INSTANCE.codes_set_double(handle, key, value);
        if (eccodesError != 0) {
            throw new EccodesException(eccodesError);
        }
    }

    public void setStringValue(String key, String value) throws EccodesException {
        int eccodesError;
        eccodesError = Eccodes.INSTANCE.codes_set_string(handle, key, value, new LongByReference(value.length()));
        if (eccodesError != 0) {
            throw new EccodesException(eccodesError);
        }
    }

    @Override
    public void finalize() throws EccodesException {
        int eccodesError;
        eccodesError = Eccodes.INSTANCE.codes_handle_delete(handle);
        if (eccodesError != 0) {
            throw new EccodesException(eccodesError);
        }
    }
}
