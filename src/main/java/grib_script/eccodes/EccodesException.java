package grib_script.eccodes;

public class EccodesException extends Exception {
    private static final long serialVersionUID = -4897274774978930600L;

    private int errorCode;

    public EccodesException(int errorCode) {
        super("Eccodes exception: " + errorCode);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return this.errorCode;
    }
}
