package grib_script.eccodes;

import java.util.ArrayList;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

import grib_script.jna.Eccodes;
import grib_script.jna.Libc;

public class Grib {
    private List<GribHandle> gribHandleList = new ArrayList<>();

    public void open(String filename) throws EccodesException {
        gribHandleList.clear();
        IntByReference err = new IntByReference(0);
        Libc.CLibrary.FILE file = Libc.INSTANCE.fopen(filename, "r");
        if (file != null) {
            Pointer handle;
            while ((handle = Eccodes.INSTANCE.codes_grib_handle_new_from_file(null, file, err)) != null) {
                gribHandleList.add(new GribHandle(handle));
            }
        } else {
            throw new EccodesException(err.getValue());
        }
    }

    public Iterable<GribHandle> gribHandleIterator() {
        return gribHandleList;
    }
}
