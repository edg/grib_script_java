grammar GribScript;

@lexer::header {
package grib_script.antlr4;
}

@parser::header {
package grib_script.antlr4;
}

SEMICOLON : ';'                   ;
ADD       : '+'                   ;
SUB       : '-'                   ;
MUL       : '*'                   ;
DIV       : '/'                   ;
INT       : [0-9]+                ;
DOUBLE    : [0-9]+'.'[0-9]+       ;
ID        : [a-zA-Z_][a-zA-Z0-9_]*;
LPAR      : '('                   ;
RPAR      : ')'                   ;
EQ        : '='                   ;
AND		  : '&&'				  ;
OR		  : '||'				  ;
TRUE	  : 'true'                ;
FALSE     : 'false'               ;
ISEQ  	  : '=='				  ;
NEQ		  : '!='                  ;
GT		  : '>'                   ;
GTE		  : '>='				  ;
LT		  : '<'                   ;
LTE		  : '<='				  ;
NOT       : '!'                   ;


STRING     : '"' ('\\"' | ~('\n'|'\r'|'"'))* '"';
WHITESPACE : ' '         -> skip ;

NEWLINE    : ('\n'|'\r') -> skip ;
TAB		   : '\t'        -> skip ;
COMMENT	   : '//' ~('\n'|'\r')* ('\n'|'\r') -> skip ;


program
	: sections EOF
	;
	
sections
	: section*
    ;
	
section
	: beginsection						# BeginSection
	| matchsection						# MatchSection
	| valuesection					    # ValueSection
	| endsection						# EndSection
	;
    
beginsection
	: 'BEGIN' '{' statements '}'
	;
	
matchsection
	: expr? '{' statements '}'
	;
	
valuesection
	: 'GRIBVALUE' expr? '{' statements '}'
	;
	
endsection
	: 'END' '{' statements '}'
	;
	
statements
	: statement*
	;

statement
    : ID EQ expr SEMICOLON          # Assignment
    | ifelse                        # IfElse
    | function SEMICOLON            # FunctionStatement
    ;

ifelse
    : 'if' ifblock ('elif' ifblock)* ('else' elsestatements)?
    ;

ifblock
    : expr ifstatements
    ;

ifstatements
    : '{' statements '}'
    ;

elsestatements
    : ifstatements
    ;

expr
	: unaryop expr                  # UnaryExpr
	| expr muldivop expr			# MulDivExpr
	| expr addsubop expr        	# AddSubExpr
	| expr boolop expr              # BoolExpr
	| atom                          # AtomExpr
	;
	
muldivop
	: MUL							# Mul
	| DIV							# Div
	;
	
addsubop
	: ADD							# Add
	| SUB							# Sub
	;
	
boolop
	: AND							# And
	| OR							# Or
	| ISEQ							# Eq
	| NEQ							# Neq
	| GT							# Gt
	| GTE							# Gte
	| LT							# Lt
	| LTE							# Lte
	;
	
unaryop
    : NOT                           # Not
    ;

atom
    : INT                           # Int
    | DOUBLE                        # Double
    | STRING                        # String
    | ID                            # Id
    | function                      # FunctionAtom
    | LPAR expr RPAR                # Parens
    ;

function
    : ID LPAR functionargs? RPAR
    ;

functionargs
	: expr (',' expr)*
	;
