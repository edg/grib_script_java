package grib_script.interpreter.evaluator.function;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;

public class WriteFunctionTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    /**
     * Test write function (no append)
     * 
     * @throws IOException
     * @throws FunctionEvaluatorException
     */
    @Test
    public void test001() throws IOException, FunctionEvaluatorException {
        File filename = folder.newFile();
        new WriteFunction().evaluate(new FunctionArguments(
                Arrays.asList(new StringValue(filename.getPath()), new BooleanValue(false), new StringValue("test"))));
        assertTrue(filename.exists());
        assertEquals("test", new String(Files.readAllBytes(filename.toPath())));
    }

    /**
     * Test write function (append)
     * 
     * @throws IOException
     * @throws FunctionEvaluatorException
     */
    @Test
    public void test002() throws IOException, FunctionEvaluatorException {
        File filename = folder.newFile();
        new WriteFunction().evaluate(new FunctionArguments(
                Arrays.asList(new StringValue(filename.getPath()), new BooleanValue(false), new StringValue("test1"))));
        assertTrue(filename.exists());
        assertEquals("test1", new String(Files.readAllBytes(filename.toPath())));

        new WriteFunction().evaluate(new FunctionArguments(
                Arrays.asList(new StringValue(filename.getPath()), new BooleanValue(true), new StringValue("test2"))));
        assertTrue(filename.exists());
        assertEquals("test1test2", new String(Files.readAllBytes(filename.toPath())));
    }

    /**
     * Test write function (append as integer, message as double)
     * 
     * @throws IOException
     * @throws FunctionEvaluatorException
     */
    @Test
    public void test003() throws IOException, FunctionEvaluatorException {
        File filename = folder.newFile();
        new WriteFunction().evaluate(new FunctionArguments(
                Arrays.asList(new StringValue(filename.getPath()), new IntegerValue(0), new DoubleValue(0.123))));
        assertTrue(filename.exists());
        assertTrue(new String(Files.readAllBytes(filename.toPath())).matches("^0?\\.1230*$"));

        new WriteFunction().evaluate(new FunctionArguments(
                Arrays.asList(new StringValue(filename.getPath()), new IntegerValue(1), new StringValue("test2"))));
        assertTrue(filename.exists());
        assertTrue(new String(Files.readAllBytes(filename.toPath())).matches("^0?\\.1230*test2$"));

        new WriteFunction().evaluate(new FunctionArguments(
                Arrays.asList(new StringValue(filename.getPath()), new IntegerValue(0), new StringValue("test3"))));
        assertTrue(filename.exists());
        assertEquals("test3", new String(Files.readAllBytes(filename.toPath())));
    }
}
