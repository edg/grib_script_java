package grib_script.interpreter.evaluator.expression;

import static org.junit.Assert.*;

import java.util.Arrays;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import grib_script.ast.node.Expression;
import grib_script.eccodes.Grib;
import grib_script.eccodes.GribHandle;
import grib_script.interpreter.evaluator.context.Context;
import grib_script.interpreter.evaluator.expression.ExpressionEvaluatorVisitor;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.GribValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;
import grib_script.utils.ExpressionStringAndValuePair;
import grib_script.utils.StringParser;

@RunWith(Parameterized.class)
public class ExpressionEvaluatorFileTest {
    private Context context;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Parameter
    public ExpressionStringAndValuePair parameter;

    @Before
    public void setUp() throws Exception {
        context = new Context();
        String filename = this.getClass().getClassLoader().getResource("erg5.grib2").getFile();
        Grib grib = new Grib();
        grib.open(filename);
        GribHandle h = grib.gribHandleIterator().iterator().next();
        context.setVariable("CURRENT_GRIB", new GribValue(h));
    }

    @Parameters(name = "Test expression {0}")
    public static Iterable<ExpressionStringAndValuePair> getParameters() {
        ExpressionStringAndValuePair[] parameters = {
                // Integer SUM
                new ExpressionStringAndValuePair("1 + 1", new IntegerValue(2)),
                new ExpressionStringAndValuePair("1 + 1.0", new DoubleValue(2.0)),
                new ExpressionStringAndValuePair("1 + \"ciao\""), new ExpressionStringAndValuePair("1 + true"),
                // Integer SUB
                new ExpressionStringAndValuePair("1 - 1", new IntegerValue(0)),
                new ExpressionStringAndValuePair("1 - 1.0", new DoubleValue(0.0)),
                new ExpressionStringAndValuePair("1 - \"ciao\""), new ExpressionStringAndValuePair("1 - true"),
                // Integer MUL
                new ExpressionStringAndValuePair("1 * 1", new IntegerValue(1)),
                new ExpressionStringAndValuePair("1 * 1.0", new DoubleValue(1.0)),
                new ExpressionStringAndValuePair("1 * \"ciao\""), new ExpressionStringAndValuePair("1 * true"),
                // Integer DIV
                new ExpressionStringAndValuePair("1 / 1", new IntegerValue(1)),
                new ExpressionStringAndValuePair("1 / 1.0", new DoubleValue(1.0)),
                new ExpressionStringAndValuePair("1 / \"ciao\""), new ExpressionStringAndValuePair("1 / true"),
                // Integer EQ
                new ExpressionStringAndValuePair("1 == 1", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 == 1.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 == \"ciao\""), new ExpressionStringAndValuePair("1 == true"),
                // Integer NEQ
                new ExpressionStringAndValuePair("1 != 2", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 != 2.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 != \"ciao\""), new ExpressionStringAndValuePair("1 != true"),
                // Integer LT
                new ExpressionStringAndValuePair("1 < 2", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 < 2.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 < \"ciao\""), new ExpressionStringAndValuePair("1 < true"),
                // Integer LTE
                new ExpressionStringAndValuePair("1 <= 1", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 <= 1.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 <= \"ciao\""), new ExpressionStringAndValuePair("1 <= true"),
                // Integer GT
                new ExpressionStringAndValuePair("2 > 1", new BooleanValue(true)),
                new ExpressionStringAndValuePair("2 > 1.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 > \"ciao\""), new ExpressionStringAndValuePair("1 > true"),
                // Integer GTE
                new ExpressionStringAndValuePair("1 >= 1", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 >= 1.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 >= \"ciao\""), new ExpressionStringAndValuePair("1 >= true"),
                // Integer AND
                new ExpressionStringAndValuePair("1 && 1", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 && 1.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 && \"ciao\"", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 && true", new BooleanValue(true)),
                // Integer OR
                new ExpressionStringAndValuePair("1 || 1", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 || 1.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 || \"ciao\"", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1 || true", new BooleanValue(true)),
                // Double SUM
                new ExpressionStringAndValuePair("1.0 + 1", new DoubleValue(2.0)),
                new ExpressionStringAndValuePair("1.0 + 1.0", new DoubleValue(2.0)),
                new ExpressionStringAndValuePair("1.0 + \"ciao\""), new ExpressionStringAndValuePair("1.0 + true"),
                // Double SUB
                new ExpressionStringAndValuePair("1.0 - 1", new DoubleValue(0.0)),
                new ExpressionStringAndValuePair("1.0 - 1.0", new DoubleValue(0.0)),
                new ExpressionStringAndValuePair("1.0 - \"ciao\""), new ExpressionStringAndValuePair("1.0 - true"),
                // Double MUL
                new ExpressionStringAndValuePair("1.0 * 1", new DoubleValue(1.0)),
                new ExpressionStringAndValuePair("1.0 * 1.0", new DoubleValue(1.0)),
                new ExpressionStringAndValuePair("1.0 * \"ciao\""), new ExpressionStringAndValuePair("1.0 * true"),
                // Double DIV
                new ExpressionStringAndValuePair("1.0 / 1", new DoubleValue(1.0)),
                new ExpressionStringAndValuePair("1.0 / 1.0", new DoubleValue(1.0)),
                new ExpressionStringAndValuePair("1.0 / \"ciao\""), new ExpressionStringAndValuePair("1.0 / true"),
                // Double EQ
                new ExpressionStringAndValuePair("1.0 == 1", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1.0 == 1.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1.0 == \"ciao\""), new ExpressionStringAndValuePair("1.0 == true"),
                // Double NEQ
                new ExpressionStringAndValuePair("1.0 != 2", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1.0 != 2.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1.0 == \"ciao\""), new ExpressionStringAndValuePair("1.0 == true"),
                // Double LT
                new ExpressionStringAndValuePair("1.0 < 2", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1.0 < 2.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1.0 < \"ciao\""), new ExpressionStringAndValuePair("1.0 < true"),
                // Double LTE
                new ExpressionStringAndValuePair("1.0 <= 1", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1.0 <= 1.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1.0 <= \"ciao\""), new ExpressionStringAndValuePair("1.0 <= true"),
                // Double GT
                new ExpressionStringAndValuePair("2.0 > 1", new BooleanValue(true)),
                new ExpressionStringAndValuePair("2.0 > 1.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1.0 > \"ciao\""), new ExpressionStringAndValuePair("1.0 > true"),
                // Double GTE
                new ExpressionStringAndValuePair("1.0 >= 1", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1.0 >= 1.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("1.0 >= \"ciao\""), new ExpressionStringAndValuePair("1.0 >= true"),
                // String SUM
                new ExpressionStringAndValuePair("\"ciao\" + 1", new StringValue("ciao1")),
                new ExpressionStringAndValuePair("\"ciao\" + 1.0", new StringValue("ciao1.0")),
                new ExpressionStringAndValuePair("\"ciao\" + true"),
                new ExpressionStringAndValuePair("\"ciao\" + \"ciao\"", new StringValue("ciaociao")),
                // String SUB
                new ExpressionStringAndValuePair("\"ciao\" - 1"), new ExpressionStringAndValuePair("\"ciao\" - 1.0"),
                new ExpressionStringAndValuePair("\"ciao\" - true"),
                new ExpressionStringAndValuePair("\"ciao\" - \"ciao\""),
                // String MUL
                new ExpressionStringAndValuePair("\"ciao\" * 1"), new ExpressionStringAndValuePair("\"ciao\" * 1.0"),
                new ExpressionStringAndValuePair("\"ciao\" * true"),
                new ExpressionStringAndValuePair("\"ciao\" * \"ciao\""),
                // String DIV
                new ExpressionStringAndValuePair("\"ciao\" / 1"), new ExpressionStringAndValuePair("\"ciao\" / 1.0"),
                new ExpressionStringAndValuePair("\"ciao\" / true"),
                new ExpressionStringAndValuePair("\"ciao\" / \"ciao\""),
                // String EQ
                new ExpressionStringAndValuePair("\"ciao\" == 1"), new ExpressionStringAndValuePair("\"ciao\" == 1.0"),
                new ExpressionStringAndValuePair("\"ciao\" == true"),
                new ExpressionStringAndValuePair("\"ciao\" == \"ciao\"", new BooleanValue(true)),
                // String NEQ
                new ExpressionStringAndValuePair("\"ciao\" != 1"), new ExpressionStringAndValuePair("\"ciao\" != 1.0"),
                new ExpressionStringAndValuePair("\"ciao\" != true"),
                new ExpressionStringAndValuePair("\"ciao\" != \"ciao\"", new BooleanValue(false)),
                // String LT
                new ExpressionStringAndValuePair("\"ciao\" < 1"), new ExpressionStringAndValuePair("\"ciao\" < 1.0"),
                new ExpressionStringAndValuePair("\"ciao\" < true"),
                new ExpressionStringAndValuePair("\"ciao\" < \"ciao\"", new BooleanValue(false)),
                // String LTE
                new ExpressionStringAndValuePair("\"ciao\" < 1"), new ExpressionStringAndValuePair("\"ciao\" < 1.0"),
                new ExpressionStringAndValuePair("\"ciao\" < true"),
                new ExpressionStringAndValuePair("\"ciao\" <= \"ciao\"", new BooleanValue(true)),
                // String GT
                new ExpressionStringAndValuePair("\"ciao\" > 1"), new ExpressionStringAndValuePair("\"ciao\" > 1.0"),
                new ExpressionStringAndValuePair("\"ciao\" > true"),
                new ExpressionStringAndValuePair("\"ciao\" > \"ciao\"", new BooleanValue(false)),
                // String GTE
                new ExpressionStringAndValuePair("\"ciao\" >= 1"), new ExpressionStringAndValuePair("\"ciao\" >= 1.0"),
                new ExpressionStringAndValuePair("\"ciao\" >= true"),
                new ExpressionStringAndValuePair("\"ciao\" >= \"ciao\"", new BooleanValue(true)),
                // Boolean SUM
                new ExpressionStringAndValuePair("true + 1"), new ExpressionStringAndValuePair("true + 1.0"),
                new ExpressionStringAndValuePair("true + true"), new ExpressionStringAndValuePair("true + \"ciao\""),
                // Boolean EQ
                new ExpressionStringAndValuePair("true == 1", new BooleanValue(true)),
                new ExpressionStringAndValuePair("true == 1.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("true == true", new BooleanValue(true)),
                new ExpressionStringAndValuePair("true == \"\"", new BooleanValue(true)),
                // Boolean NEQ
                new ExpressionStringAndValuePair("true != 0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("true != 0.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("true != false", new BooleanValue(true)),
                new ExpressionStringAndValuePair("true == \"notempty\"", new BooleanValue(true)),
                // Boolean AND
                new ExpressionStringAndValuePair("true && 1", new BooleanValue(true)),
                new ExpressionStringAndValuePair("true && 1.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("true && true", new BooleanValue(true)),
                new ExpressionStringAndValuePair("true && \"\"", new BooleanValue(true)),
                // Boolean OR
                new ExpressionStringAndValuePair("true || 0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("true || 0.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("true || false", new BooleanValue(true)),
                new ExpressionStringAndValuePair("true || \"notempty\"", new BooleanValue(true)),
                // GribKey SUM
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") + 1", new IntegerValue(201)),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") + 1.0", new DoubleValue(201.0)),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") + true"),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") + \"_string\"",
                        new StringValue("200_string")),
                // GribKey SUB
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") - 1", new IntegerValue(199)),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") - 1.0", new DoubleValue(199.0)),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") - true"),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") - \"_string\""),
                // GribKey MUL
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") * 2", new IntegerValue(400)),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") * 2.0", new DoubleValue(400.0)),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") * true"),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") * \"_string\""),
                // GribKey DIV
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") / 2", new IntegerValue(100)),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") / 2.0", new DoubleValue(100.0)),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") / true"),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") / \"_string\""),
                // GribKey EQ
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") == 200", new BooleanValue(true)),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") == 200.0", new BooleanValue(true)),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") == true", new BooleanValue(true)),
                new ExpressionStringAndValuePair("getgrib(CURRENT_GRIB, \"centre\") == \"200\"",
                        new BooleanValue(true)), };
        return Arrays.asList(parameters);
    }

    @Test
    public void test() {
        if (parameter.expectedValue == null) {
            exception.expect(RuntimeException.class);
        }
        Expression expression = StringParser.fromString(parameter.inputString).parseExpression();
        ExpressionEvaluatorVisitor evaluator = new ExpressionEvaluatorVisitor(context);
        Value result = expression.accept(evaluator);
        assertEquals(parameter.expectedValue.toString(), result.toString());
    }

}
