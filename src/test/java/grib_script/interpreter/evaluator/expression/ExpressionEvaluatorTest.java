package grib_script.interpreter.evaluator.expression;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import grib_script.ast.node.BinaryOperationExpression;
import grib_script.ast.node.BinaryOperationType;
import grib_script.ast.node.BooleanExpression;
import grib_script.ast.node.DoubleExpression;
import grib_script.ast.node.IntegerExpression;
import grib_script.ast.node.StringExpression;
import grib_script.ast.node.VariableExpression;
import grib_script.interpreter.evaluator.context.Context;
import grib_script.interpreter.evaluator.expression.ExpressionEvaluatorVisitor;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.Value;

public class ExpressionEvaluatorTest {
    private Context context;
    private ExpressionEvaluatorVisitor evaluator;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() {
        context = new Context();
        context.setVariable("integerVariable", new IntegerValue(2));
        evaluator = new ExpressionEvaluatorVisitor(context);
    }

    /**
     * 12 + 2 = 14
     */
    @Test
    public void test0001() {
        Value value = new BinaryOperationExpression(new IntegerExpression(12), new IntegerExpression(2),
                BinaryOperationType.SUM).accept(this.evaluator);
        assertEquals(IntegerValue.class, value.getClass());
        assertEquals(new Integer(14), ((IntegerValue) value).getValue());
    }

    /**
     * 12 + 2.0 = 14.0
     */
    @Test
    public void test0002() {
        Value value = new BinaryOperationExpression(new IntegerExpression(12), new DoubleExpression(2.0),
                BinaryOperationType.SUM).accept(this.evaluator);
        assertEquals(DoubleValue.class, value.getClass());
        assertEquals(new Double(14.0), ((DoubleValue) value).getValue());
    }

    /**
     * 12 + true => error
     */
    @Test
    public void test0003() {
        exception.expect(RuntimeException.class);
        new BinaryOperationExpression(new IntegerExpression(12), new BooleanExpression(true), BinaryOperationType.SUM)
                .accept(this.evaluator);
    }

    /**
     * 12 + "2" => error
     */
    @Test
    public void test0004() {
        exception.expect(RuntimeException.class);
        new BinaryOperationExpression(new IntegerExpression(12), new StringExpression("2"), BinaryOperationType.SUM)
                .accept(this.evaluator);
    }

    /**
     * integerVariable = 2 12 + integerVariable = 14
     */
    @Test
    public void test0005() {
        Value value = new BinaryOperationExpression(new IntegerExpression(12),
                new VariableExpression("integerVariable"), BinaryOperationType.SUM).accept(this.evaluator);
        assertEquals(IntegerValue.class, value.getClass());
        assertEquals(new Integer(14), ((IntegerValue) value).getValue());
    }
}
