package grib_script.interpreter.evaluator.binaryoperation;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import grib_script.ast.node.BinaryOperationType;
import grib_script.eccodes.EccodesException;
import grib_script.eccodes.Grib;
import grib_script.eccodes.GribHandle;
import grib_script.interpreter.evaluator.value.BooleanValue;
import grib_script.interpreter.evaluator.value.DoubleValue;
import grib_script.interpreter.evaluator.value.GribKeyValue;
import grib_script.interpreter.evaluator.value.GribValue;
import grib_script.interpreter.evaluator.value.IntegerValue;
import grib_script.interpreter.evaluator.value.StringValue;
import grib_script.interpreter.evaluator.value.Value;

public class BinaryOperationEvaluatorTest {
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private GribHandle createGribHandle() {
        String filename = this.getClass().getClassLoader().getResource("erg5.grib2").getFile();
        Grib grib = new Grib();
        try {
            grib.open(filename);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return grib.gribHandleIterator().iterator().next();
    }

    /**
     * shortName == "t" -> true
     * 
     * @throws Exception
     */
    @Test
    public void test001() throws Exception {
        GribKeyValue left = new GribKeyValue("shortName", createGribHandle());
        StringValue right = new StringValue("t");
        Value result = new BinaryOperationEvaluator(BinaryOperationType.EQ, left, right).eval();
        assertTrue(result instanceof BooleanValue);
        assertEquals(true, result.toBooleanValue().getValue());
    }

    /**
     * editionNumber == 2 -> true
     * 
     * @throws Exception
     */
    @Test
    public void test002() throws Exception {
        GribKeyValue left = new GribKeyValue("editionNumber", createGribHandle());
        IntegerValue right = new IntegerValue(2);
        Value result = new BinaryOperationEvaluator(BinaryOperationType.EQ, left, right).eval();
        assertTrue(result instanceof BooleanValue);
        assertEquals(true, result.toBooleanValue().getValue());
    }

    /**
     * latitudeOfFirstGridPointInDegrees == 43.6425 -> true
     * 
     * @throws Exception
     */
    @Test
    public void test003() throws Exception {
        GribKeyValue left = new GribKeyValue("latitudeOfFirstGridPointInDegrees", createGribHandle());
        DoubleValue right = new DoubleValue(43.6425);
        Value result = new BinaryOperationEvaluator(BinaryOperationType.EQ, left, right).eval();
        assertTrue(result instanceof BooleanValue);
        assertEquals(true, result.toBooleanValue().getValue());
    }

    /**
     * latitudeOfFirstGridPointInDegrees == false -> false
     * 
     * @throws Exception
     */
    @Test
    public void test004() throws Exception {
        GribKeyValue left = new GribKeyValue("latitudeOfFirstGridPointInDegrees", createGribHandle());
        BooleanValue right = new BooleanValue(false);
        Value result = new BinaryOperationEvaluator(BinaryOperationType.EQ, left, right).eval();
        assertTrue(result instanceof BooleanValue);
        assertEquals(false, result.toBooleanValue().getValue());
    }

    /**
     * iScansNegatively == false -> true (iScansNegatively == 0)
     * 
     * @throws Exception
     */
    @Test
    public void test005() throws Exception {
        GribKeyValue left = new GribKeyValue("iScansNegatively", createGribHandle());
        BooleanValue right = new BooleanValue(false);
        Value result = new BinaryOperationEvaluator(BinaryOperationType.EQ, left, right).eval();
        assertTrue(result instanceof BooleanValue);
        assertEquals(true, result.toBooleanValue().getValue());
    }

    /**
     * 1.0 + 2 = 3.0
     */
    @Test
    public void test006() throws BinaryOperationException {
        DoubleValue left = new DoubleValue(1.0);
        IntegerValue right = new IntegerValue(2);
        Value result = new BinaryOperationEvaluator(BinaryOperationType.SUM, left, right).eval();
        assertTrue(result instanceof DoubleValue);
        assertEquals(new Double(3.0), result.toDoubleValue().getValue());
    }

    /**
     * 1.0 + 2.0 = 3.0
     */
    @Test
    public void test007() throws BinaryOperationException {
        DoubleValue left = new DoubleValue(1.0);
        DoubleValue right = new DoubleValue(2.0);
        Value result = new BinaryOperationEvaluator(BinaryOperationType.SUM, left, right).eval();
        assertTrue(result instanceof DoubleValue);
        assertEquals(new Double(3.0), result.toDoubleValue().getValue());
    }

    /**
     * 1.0 + true -> error
     */
    @Test
    public void test008() throws BinaryOperationException {
        DoubleValue left = new DoubleValue(1.0);
        BooleanValue right = new BooleanValue(true);
        exception.expect(BinaryOperationException.class);
        new BinaryOperationEvaluator(BinaryOperationType.SUM, left, right).eval();
    }

    /**
     * 1.0 + "string" -> error
     */
    @Test
    public void test009() throws BinaryOperationException {
        DoubleValue left = new DoubleValue(1.0);
        StringValue right = new StringValue("string");
        exception.expect(BinaryOperationException.class);
        new BinaryOperationEvaluator(BinaryOperationType.SUM, left, right).eval();
    }

    /**
     * "string" + 1 = "string1"
     */
    @Test
    public void test010() throws BinaryOperationException {
        StringValue left = new StringValue("string");
        IntegerValue right = new IntegerValue(1);
        Value result = new BinaryOperationEvaluator(BinaryOperationType.SUM, left, right).eval();
        assertTrue(result instanceof StringValue);
        assertEquals("string1", result.toStringValue().getValue());
    }

    /**
     * "string" + shortName = "stringt"
     */
    @Test
    public void test011() throws BinaryOperationException {
        StringValue left = new StringValue("string");
        GribKeyValue right = new GribKeyValue("shortName", createGribHandle());
        Value result = new BinaryOperationEvaluator(BinaryOperationType.SUM, left, right).eval();
        assertTrue(result instanceof StringValue);
        assertEquals("stringt", result.toStringValue().getValue());
    }

    /**
     * 10 + centre = 210
     */
    @Test
    public void test012() throws BinaryOperationException {
        IntegerValue left = new IntegerValue(10);
        GribKeyValue right = new GribKeyValue("centre", createGribHandle());
        Value result = new BinaryOperationEvaluator(BinaryOperationType.SUM, left, right).eval();
        assertTrue(result instanceof IntegerValue);
        assertEquals(new Integer(210), result.toIntegerValue().getValue());
    }

    /**
     * 10.0 + centre = 210.0
     */
    @Test
    public void test013() throws BinaryOperationException {
        DoubleValue left = new DoubleValue(10.0);
        GribKeyValue right = new GribKeyValue("centre", createGribHandle());
        Value result = new BinaryOperationEvaluator(BinaryOperationType.SUM, left, right).eval();
        assertTrue(result instanceof DoubleValue);
        assertEquals(new Double(210.0), result.toDoubleValue().getValue());
    }

    /**
     * GribValue + GribValue
     * 
     * @throws BinaryOperationException
     * @throws EccodesException
     */
    @Test
    public void test014() throws BinaryOperationException, EccodesException {
        GribValue left = new GribValue(createGribHandle());
        GribValue right = new GribValue(createGribHandle());

        int gribIndex = 166;
        Value result = new BinaryOperationEvaluator(BinaryOperationType.SUM, left, right).eval();
        assertTrue(result instanceof GribValue);
        assertEquals((left.getGribHandle().getData().get(gribIndex) + right.getGribHandle().getData().get(gribIndex)),
                (GribValue.class.cast(result).getGribHandle().getData().get(gribIndex).doubleValue()), 0);

        assertEquals(left.getGribHandle().getMissingValue(),
                (GribValue.class.cast(result).getGribHandle().getData().get(0)));
    }
}
