package grib_script.eccodes;

import static org.junit.Assert.*;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class GribHandleTest {
    Grib grib;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        String filename = this.getClass().getClassLoader().getResource("erg5.grib2").getFile();
        grib = new Grib();
        grib.open(filename);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test001() throws Exception {
        GribHandle h = grib.gribHandleIterator().iterator().next();
        assertEquals(2400, h.getData().size());
        List<Double> newData = h.getData().stream().map(v -> v + 1.0).collect(Collectors.toList());
        h.setData(newData);
        assertEquals(newData.get(0), h.getData().get(0), 0.1);
    }

    @Test
    public void test002() throws EccodesException {
        exception.expect(EccodesException.class);

        GribHandle h = grib.gribHandleIterator().iterator().next();
        assertEquals("x", h.getStringValue("ciccioriccio"));
    }

    @Test
    public void test003() throws EccodesException {
        GribHandle h = grib.gribHandleIterator().iterator().next();
        h.setLongValue("centre", new Long(98));
        assertEquals(new Long(98), h.getLongValue("centre"));
    }

    @Test
    public void test004() throws EccodesException {
        GribHandle h = grib.gribHandleIterator().iterator().next();
        h.setDoubleValue("centre", new Double(98.0));
        assertEquals(new Long(98), h.getLongValue("centre"));
    }

    @Test
    public void test005() throws EccodesException {
        GribHandle h = grib.gribHandleIterator().iterator().next();
        h.setStringValue("centre", new String("ecmf"));
        assertEquals(new Long(98), h.getLongValue("centre"));
        assertEquals("ecmf", h.getStringValue("centre"));
    }
}
