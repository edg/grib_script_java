package grib_script;

import java.io.IOException;

import java.util.Arrays;

import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.RecognitionException;

import grib_script.antlr4.GribScriptLexer;
import grib_script.antlr4.GribScriptParser;

@RunWith(Parameterized.class)
public class ValidInputTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Parameter
    public String inputFilename;

    @Parameters(name = "Test input file {0}")
    public static Iterable<String> inputFilenames() {
        String[] filenames = { "valid_001", "valid_002" };
        return Arrays.asList(filenames);
    }

    @Test
    public void testInputfile() throws IOException {
        CharStream in = CharStreams.fromStream(this.getClass().getClassLoader().getResourceAsStream(inputFilename));
        GribScriptLexer lexer = new GribScriptLexer(in);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        GribScriptParser parser = new GribScriptParser(tokens);
        lexer.removeErrorListeners();
        lexer.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int pos, String msg,
                    RecognitionException e) {
                throw new IllegalStateException("Failed to parse at " + line + "," + pos + ":  " + msg, e);
            }
        });
        parser.removeErrorListeners();
        parser.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int pos, String msg,
                    RecognitionException e) {
                throw new IllegalStateException("Failed to parse at " + line + "," + pos + ":  " + msg, e);
            }
        });
        parser.program();
    }
}
