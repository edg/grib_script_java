package grib_script.ast.builder;

import static org.junit.Assert.assertEquals;

import grib_script.ast.node.*;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.junit.Test;

import grib_script.antlr4.GribScriptLexer;
import grib_script.antlr4.GribScriptParser;
import grib_script.antlr4.GribScriptParser.ProgramContext;

public class ProgramBuilderTest {
    private Program parseProgram(String input) {
        CharStream in = CharStreams.fromString(input);
        GribScriptLexer lexer = new GribScriptLexer(in);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        GribScriptParser parser = new GribScriptParser(tokens);
        lexer.removeErrorListeners();
        lexer.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int pos, String msg,
                    RecognitionException e) {
                throw new IllegalStateException("Failed to parse at " + line + "," + pos + ":  " + msg, e);
            }
        });
        parser.removeErrorListeners();
        parser.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int pos, String msg,
                    RecognitionException e) {
                throw new IllegalStateException("Failed to parse at " + line + "," + pos + ":  " + msg, e);
            }
        });
        ProgramContext programContext = parser.program();
        ProgramBuilder builder = new ProgramBuilder();
        return programContext.accept(builder);
    }

    @Test
    public void testValidInput001() {
        Program program = parseProgram("BEGIN {}");
        assertEquals(1, program.getSections().size());
        Section section = program.getSections().get(0);
        assertEquals(section.getClass(), BeginSection.class);
    }

    @Test
    public void testValidInput002() {
        Program program = parseProgram("END {}");
        assertEquals(1, program.getSections().size());
        Section section = program.getSections().get(0);
        assertEquals(section.getClass(), EndSection.class);
    }

    @Test
    public void testValidInput003() {
        Program program = parseProgram("BEGIN {} \n {} \n BEGIN {}");
        assertEquals(3, program.getSections().size());
        assertEquals(program.getSections().get(0).getClass(), BeginSection.class);
        assertEquals(program.getSections().get(1).getClass(), MatchSection.class);
        assertEquals(program.getSections().get(2).getClass(), BeginSection.class);
    }

    @Test
    public void testValidInput004() {
        Program program = parseProgram("{}");
        assertEquals(1, program.getSections().size());
        assertEquals(program.getSections().get(0).getClass(), MatchSection.class);
    }

    @Test
    public void testValidInput007() {
        Program program = parseProgram("BEGIN {} END {} shortName == \"t\" {} {} {} END {}");
        assertEquals(6, program.getSections().size());
        assertEquals(program.getSections().get(0).getClass(), BeginSection.class);
        assertEquals(program.getSections().get(1).getClass(), EndSection.class);
        assertEquals(program.getSections().get(2).getClass(), MatchSection.class);
        assertEquals(program.getSections().get(3).getClass(), MatchSection.class);
        assertEquals(program.getSections().get(4).getClass(), MatchSection.class);
        assertEquals(program.getSections().get(5).getClass(), EndSection.class);
    }

    @Test
    public void testValidInput008() {
        Program program = parseProgram("BEGIN { x=1+2; foo(1,2.0,\"bar baz\", foo(), x); }");
        Section section = program.getSections().get(0);
        assertEquals(2, section.getStatements().size());
        // x = 1 + 2
        Statement statement = section.getStatements().get(0);
        assertEquals(Assignment.class, statement.getClass());
        Assignment assignment = (Assignment) statement;
        assertEquals("x", assignment.getName());
        assertEquals(BinaryOperationExpression.class, assignment.getExpression().getClass());
        BinaryOperationExpression binaryOperationExpression = (BinaryOperationExpression) assignment.getExpression();
        assertEquals(IntegerExpression.class, binaryOperationExpression.getLeft().getClass());
        assertEquals(1, ((IntegerExpression) binaryOperationExpression.getLeft()).getValue());
        assertEquals(IntegerExpression.class, binaryOperationExpression.getRight().getClass());
        assertEquals(2, ((IntegerExpression) binaryOperationExpression.getRight()).getValue());
        assertEquals(BinaryOperationType.SUM, binaryOperationExpression.getOperation());
        // foo(1,2,3)
        statement = section.getStatements().get(1);
        assertEquals(Function.class, statement.getClass());
        Function function = (Function) statement;
        assertEquals("foo", function.getName());
        assertEquals(5, function.getArgs().size());

        assertEquals(IntegerExpression.class, function.getArgs().get(0).getClass());
        assertEquals(1, ((IntegerExpression) function.getArgs().get(0)).getValue());

        assertEquals(DoubleExpression.class, function.getArgs().get(1).getClass());
        assertEquals(2.0, ((DoubleExpression) function.getArgs().get(1)).getValue(), 0);

        assertEquals(StringExpression.class, function.getArgs().get(2).getClass());
        assertEquals("\"bar baz\"", ((StringExpression) function.getArgs().get(2)).getValue());
        assertEquals("bar baz", ((StringExpression) function.getArgs().get(2)).getUnquotedValue());

        assertEquals(Function.class, function.getArgs().get(3).getClass());
        assertEquals("foo", ((Function) function.getArgs().get(3)).getName());
        assertEquals(0, ((Function) function.getArgs().get(3)).getArgs().size());

        assertEquals(VariableExpression.class, function.getArgs().get(4).getClass());
        assertEquals("x", ((VariableExpression) function.getArgs().get(4)).getName());
    }

    @Test
    public void testValidInput009() {
        Program program = parseProgram("{ x = true || false && 1 + 1; }");
        Section section = program.getSections().get(0);
        assertEquals(1, section.getStatements().size());
        Statement statement = section.getStatements().get(0);
        // x = ...
        assertEquals(Assignment.class, statement.getClass());
        Assignment assignment = (Assignment) statement;
        assertEquals("x", assignment.getName());
        assertEquals(BinaryOperationExpression.class, assignment.getExpression().getClass());
        BinaryOperationExpression binaryOperationExpression = (BinaryOperationExpression) assignment.getExpression();
        // ... && ...
        assertEquals(BinaryOperationType.AND, binaryOperationExpression.getOperation());
        assertEquals(BinaryOperationExpression.class, binaryOperationExpression.getLeft().getClass());
        assertEquals(BinaryOperationExpression.class, binaryOperationExpression.getRight().getClass());
        // true || false
        assertEquals(BinaryOperationType.OR,
                ((BinaryOperationExpression) binaryOperationExpression.getLeft()).getOperation());
        assertEquals(BooleanExpression.class,
                ((BinaryOperationExpression) binaryOperationExpression.getLeft()).getLeft().getClass());
        assertEquals(true,
                ((BooleanExpression) (((BinaryOperationExpression) binaryOperationExpression.getLeft()).getLeft()))
                        .getValue());
        assertEquals(BooleanExpression.class,
                ((BinaryOperationExpression) binaryOperationExpression.getLeft()).getRight().getClass());
        assertEquals(false,
                ((BooleanExpression) (((BinaryOperationExpression) binaryOperationExpression.getLeft()).getRight()))
                        .getValue());
    }

    @Test
    public void testValidInput010() {
        Program program = parseProgram("{ if true { } }");
        Section section = program.getSections().get(0);
        assertEquals(1, section.getStatements().size());
        System.out.println(section.getStatements().get(0).getClass());
    }
}
