package grib_script.jna.eccodes;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

import grib_script.jna.Eccodes;
import grib_script.jna.Libc;

public class EccodesTest {
    @Test
    public void testEccodesVersion() {
        assertNotNull(Eccodes.INSTANCE);
        String filename = this.getClass().getClassLoader().getResource("erg5.grib2").getFile();
        Libc.CLibrary.FILE file = Libc.INSTANCE.fopen(filename, "r");
        assertNotNull(file);
        IntByReference err = new IntByReference();
        Pointer handle = Eccodes.INSTANCE.codes_grib_handle_new_from_file(null, file, err);
        assertNotEquals(0, handle);
        Libc.INSTANCE.fclose(file);
    }
}