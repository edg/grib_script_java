#!/bin/bash
set -ex

demodir=$(cd $(dirname $0) && pwd)

trap cleanup EXIT

cleanup() {
    rm scumula.grib
    rm cosmo-5M_er_*.png scumula_*.png out_*.png # out.gif
    popd
}


pushd $demodir
grib_script=../build/install/grib_script_java/bin/grib_script
$grib_script -v outfile=scumula.grib -f scumula.gribscript cosmo-5M_er.grib
python3 plot.py cosmo-5M_er.grib &
python3 plot.py scumula.grib &
wait
for index in {1..72}
do
    suffix=$(printf "%02d" $index)
    ls cosmo-5M_er_$suffix.png scumula_$suffix.png &>/dev/null || continue
    convert cosmo-5M_er_$suffix.png scumula_$suffix.png -append out_$suffix.png
done
convert -delay 30 -loop 0 out_*.png out.gif
eog out.gif
