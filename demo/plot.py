#!/usr/bin/env python3
import argparse

from Magics import macro


if __name__ =='__main__':
    parser =argparse.ArgumentParser()
    parser.add_argument("gribfile")

    args = parser.parse_args()


    europe = macro.mmap(subpage_lower_left_latitude=43.0,
                        subpage_lower_left_longitude=9.0,
                        subpage_map_projection='cylindrical',
                        subpage_upper_right_latitude=46.00,
                        subpage_upper_right_longitude=13.00)
    coast = macro.mcoast(map_grid="on",
                         map_grid_colour="tan",
                         map_coastline_colour="tan",
                         map_coastline_resolution="high")
    contour = macro.mcont(contour_automatic_setting='style_name',
                          contour_style_name='sh_blured_f05t300lst',
                          legend="on")

    for index in range(1, 73):
        print("{}_{:02d}".format(args.gribfile.split(".")[0], index))
        title = macro.mtext(text_lines=['<grib_info key="shortName"/> <grib_info key="startStep"/>-<grib_info key="endStep"/>'],
                            text_justification='left',
                            text_font_size=0.6,
                            text_colour='charcoal')
        out = macro.output(output_formats=['png'],
                           output_name_first_page_number="off",
                           output_name="{}_{:02d}".format(args.gribfile.split(".")[0], index))
        grib = macro.mgrib(grib_input_file_name=args.gribfile,
                           grib_field_position=index)

        macro.plot(out, europe, coast, grib, title, contour)
