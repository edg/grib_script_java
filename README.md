# Grib Script

Linguaggio di scripting, ispirato a [`awk`][awk] e [`grib_filter`][grib_filter].

La struttura di uno script è la seguente

```
BEGIN {
    // Eseguito all'inizio dello script
}

GRIBVALUES {
    // Eseguito per ogni valore di ogni messaggio GRIB
    // NOTA: non ancora implementato
}

(shortName == "tp" && endStep == 6) || startStep == 0 {
    // Eseguito per ogni messaggio GRIB le cui chiavi fanno match con
    // l'espressione: le variabili sono le chiavi del messaggio GRIB
    // processato.
}

{
    // Eseguito per ogni messaggio GRIB (espressione vuota quindi sempre vera)
}

END {
    // Eseguito alla fine dello script
}
```

La sintassi è uno pseudo-C simile ad `awk` e, oltre a tutto ciò che si porta
dietro un linguaggio in stile awk, ci sono funzioni e variabili built-in
orientate ai GRIB. Sono da definire, ma riguarderanno:

- Lettura e scrittura di chiavi del GRIB.
- Creazione di un GRIB.
- Scrittura di un GRIB.
- Operazioni per i valori dei GRIB
  - Somma, moltiplicazione, differenza, divisione tra un GRIB e un valore
    scalare.
  - Somma, moltiplicazione, differenza, divisione, mask tra due GRIB (che
    devono avere stesso grigliato)
  - Massimo, minimo di un GRIB.
- Generazione di una mappa (tramite Magics)

Per ora sono implementate le seguenti funzioni:

- `print(message:any)`: stampa su standard output il messaggio fornito come parametro.
- `write(filename:string, append:boolean, message:any)`: scrive sul file (in modalità append o meno)
   il messaggio.
- `writegrib(filename:string, append:boolean, value:grib) -> boolean`: scrive il grib
   su file.
- `getgrib(value:grib, key:any)`: restituisce la chiave del grib col nome specificato.

Ad esempio, la conversione da K a C di un GRIB di temperatura:

```
shortName == "t2m" {
    print("Ho trovato una temperatura!");
    // Converto i valori del GRIB attuale da K a C
    grib_in_c = CURRENT_GRIB - 273.15;
    // Scrivo il GRIB così ottenuto nel file "temperatures_in_c.grib"
    // in modalità append
    writegrib("temperatures_in_c.grib", true, grib_in_c);
}
```

I tipi esplicitamente utilizzabili sono:

- Integer
- Double
- Boolean
- String

Quelli implicitamente utilizzabili (date da variabili d'ambiente o da funzioni)
sono:

- Grib
- GribKey (valore di una chiave grib)

Le operazioni disponibili sono `+ - * / && || == !=` e assumono un significato diverso
a seconda del tipo di operandi.

Somma:

| SX/DX    | integer | double  | boolean | string  | gribkey | grib    |
|----------|---------|---------|---------|---------|---------|---------|
| integer  | integer | double  |    X    |   X     | integer |   X     |
| double   | double  | double  |    X    |   X     | double  |   X     |
| boolean  |   X     |   X     |    X    |   X     |    X    |   X     |
| string   | string  | string  |    X    | string  | string  |   X     |
| gribkey  | integer | double  |    X    | string  | integer |   X     |
| grib     | grib    | grib    |    X    |   X     |    X    | grib    |

Differenza/Moltiplicazione/Divisione:

| SX/DX    | integer | double  | boolean | string  | gribkey | grib    |
|----------|---------|---------|---------|---------|---------|---------|
| integer  | integer | double  |    X    |   X     | integer |   X     |
| double   | double  | double  |    X    |   X     | double  |   X     |
| boolean  |   X     |   X     |    X    |   X     |    X    |   X     |
| string   |   X     |   X     |    X    |   X     |    X    |   X     |
| gribkey  | integer | double  |    X    | string  | integer |   X     |
| grib     | grib    | grib    |    X    |   X     |    X    | grib    |

Operazioni booleane:

| SX/DX    | integer | double  | boolean | string  | gribkey | grib    |
|----------|---------|---------|---------|---------|---------|---------|
| integer  | boolean | boolean | boolean | boolean | boolean | 
| double   | double  | double  |    X    |   X     | double  |   X     |
| boolean  |   X     |   X     |    X    |   X     |    X    |   X     |
| string   |   X     |   X     |    X    |   X     |    X    |   X     |
| gribkey  | integer | double  |    X    | string  | integer |   X     |
| grib     | grib    | grib    |    X    |   X     |    X    | grib    |

## Gradle

Segui la [guida per progetti Java][gradle_java].

ANTLR: usa il [plugin Gradle][gradle_antlr].

[awk]: https://github.com/onetrueawk/awk
[grib_filter]: https://confluence.ecmwf.int/display/ECC/grib_filter
[gradle_java]: https://docs.gradle.org/current/userguide/building_java_projects.html
[gradle_antlr]: https://docs.gradle.org/current/userguide/antlr_plugin.html

## License

Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>.

Licensed under GNU GPLv2 or later.
